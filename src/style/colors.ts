//Define colors

export const DARK_BLUE = "#2A5686";

export const DARK_RED = "#CC0202";
export const MEDIUM_RED = "#D13C3F";
export const LIGHT_RED = "#D94040";
export const LIGHT_BLUE = "#E2EEF7";

export const LOGO_ORANGE = "#FFB70A";

// Green and red buttons on verification page
export const ERROR_RED = "#CC0000";
export const SUCCESS_GREEN = "#008000";

// Text colors
export const TEXT_LIGHT = "#FFF";
export const TEXT_DARK = "#000";

// The background colors for the text boxes on the last pages
export enum BoxColors {
    GREEN = '#048001',
    GREY = '#F2F2F2',
    YELLOW = '#FFF5CC',
}