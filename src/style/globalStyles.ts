// Import stylesheet namespace
import { StyleSheet, Platform } from "react-native";

// Import colors
import { DARK_BLUE, TEXT_DARK } from './colors'

const styles = StyleSheet.create({
    blueTextStyles: {
        color: DARK_BLUE,
        fontSize: 16,
        lineHeight: Platform.OS === 'ios' ? 23 : 25,
        fontFamily: 'lucida-grande-bold',
        marginHorizontal: 10,
    },
    text: {
        color: TEXT_DARK,
        fontSize: 16,
        includeFontPadding: false,
        lineHeight: Platform.OS === 'ios' ? 23 : 25,
        fontFamily: 'lucida-grande',
        marginBottom: 20
    },
    smallText: {
        color: TEXT_DARK,
        fontSize: 12,
        fontFamily: 'lucida-grande',
    },
    blueText: {
        flexDirection: 'row',
        alignItems: 'flex-start',
        marginBottom: 20,
    },
    title: {
        color: TEXT_DARK,
        fontSize: 18,
        lineHeight: Platform.OS === 'ios' ? 24 : 26,
        fontFamily: 'lucida-grande-bold',
        marginBottom: 10
    },
    link: {
        color: DARK_BLUE,
        fontSize: 16,
        fontFamily: 'lucida-grande',
        textDecorationLine: 'underline'
    },
    listItem: {
        color: TEXT_DARK,
        fontSize: 16,
        lineHeight: Platform.OS === 'ios' ? 23 : 25,
        fontFamily: 'lucida-grande',
        marginBottom: 12,
        marginLeft: 10
    },
    touchableListItem: {
        color: TEXT_DARK,
        fontSize: 16,
        lineHeight: Platform.OS === 'ios' ? 23 : 25,
        fontFamily: 'lucida-grande',
        marginBottom: 12,
        marginHorizontal: 10
    }
});

const getGenericStyles = () => styles;

export { getGenericStyles }