import React, { createContext, ReactNode, useState, useMemo } from 'react';

// Import aqopi types
import { Aqopi } from '@iwize-aqopi/aqopi-types';

// Types
export type AppState = {
    blobList: any[];
    summary?: Aqopi.Summary;
    summaryPartner?: Aqopi.Summary;
    pdf?: string;
    pdfPartner?: string;
    calcAmount: string;
    useStub: boolean;
    session?: any;
    hasPartner: boolean;
    currentIndex: number;
    isPartner: boolean;
    setAppState: React.Dispatch<React.SetStateAction<AppState>>
}
type AppStateProviderProps = { children: ReactNode };

// App State obj
const initialState: AppState = {
    useStub: true,
    calcAmount: '0,00',
    blobList: [],
    currentIndex: 0,
    hasPartner: false,
    isPartner: false,
    setAppState: (): void => { }
};

const AppStateContext = createContext<AppState>(initialState);

function AppStateProvider(props: AppStateProviderProps) {
    const [appState, setAppState] = useState(initialState);

    const providerValue = useMemo(() => ({ ...appState, setAppState }), [appState, setAppState]);

    return (
        <AppStateContext.Provider value={providerValue}>
            {props.children}
        </AppStateContext.Provider>
    );
};

export { AppStateProvider, AppStateContext };

