import React, { useContext } from 'react'
import { StyleSheet, View, Text } from 'react-native'

// Import colors
import { BoxColors, TEXT_DARK, TEXT_LIGHT } from '../style/colors'

// Import AppState
import { AppStateContext } from '../contexts/AppState'

// Import components
import { Check } from '../components/icons/check'

type ExtraInfoValues = {
    name: string;
    date: string;
    bankAccount: string;
    allowanceType?: string;
}

type TextBoxProps = {
    color: BoxColors;
    extraInfo?: ExtraInfoValues;
}

export function TextBox(props: TextBoxProps) {
    const { calcAmount } = useContext(AppStateContext);
    if (props.color === BoxColors.GREEN) {
        return (
            <View style={[styles.boxContainer, { backgroundColor: BoxColors.GREEN }]}>
                <View style={styles.flexRow}><Check altFill={TEXT_LIGHT} /><Text style={[styles.lightTitle, { color: TEXT_LIGHT }]}> Gelukt!</Text></View>
                <Text style={[styles.title, { color: TEXT_LIGHT }]}>Uw aanvraag is verstuurd naar de Gemeente Utrecht.</Text>
                <Text style={[styles.text, { color: TEXT_LIGHT }]}>Binnen 2 weken laat de gemeente Utrecht weten of jullie het geld krijgen.</Text>
            </View>
        )
    } else if (props.color === BoxColors.YELLOW) {
        if (!!parseFloat(calcAmount)) {
            return (
                <View style={[styles.boxContainer, { backgroundColor: BoxColors.YELLOW }]}>
                    <Text style={styles.title}>Misschien krijgt u dit jaar eenmalig €{calcAmount} </Text>
                    <Text style={styles.text}>U hoeft dit bedrag nooit meer terug te betalen</Text>
                </View>
            )
        } else {
            return (
                <View style={[styles.boxContainer, { backgroundColor: BoxColors.YELLOW }]}>
                    <Text style={styles.title}>Berekende toeslag €{calcAmount}</Text>
                    <Text style={styles.title}> </Text>
                </View>
            )
        }
    } else {
        return (
            <View style={[styles.boxContainer, { backgroundColor: BoxColors.GREY }]}>
                <Text style={styles.title}>Bedrag</Text>
                <Text style={styles.text}>€{calcAmount}</Text>
                <Text style={styles.title}>Soort </Text>
                <Text style={styles.text}>{props.extraInfo?.allowanceType || 'Individuele Inkomenstentoeslag'}</Text>
                {!!parseFloat(calcAmount) &&
                    <View>
                        <Text style={styles.title}>Uw rekeningnummer </Text>
                        <Text style={styles.text}>{props.extraInfo?.bankAccount}</Text>
                    </View>}
                <Text style={styles.title}>Uw naam </Text>
                <Text style={styles.text}>{props.extraInfo?.name}</Text>
                {!!parseFloat(calcAmount) &&
                    <View>
                        <Text style={styles.title}>Verwachte datum uitslag </Text>
                        <Text style={styles.text}>{props.extraInfo?.date}</Text>
                    </View>}
            </View>
        )
    }

}

const styles = StyleSheet.create({
    boxContainer: {
        paddingHorizontal: 15,
        paddingTop: 15,
        marginBottom: 20
    },
    text: {
        color: TEXT_DARK,
        fontSize: 16,
        lineHeight: 23,
        fontFamily: 'lucida-grande',
        marginBottom: 15
    },
    lightTitle: {
        color: TEXT_DARK,
        fontSize: 16,
        lineHeight: 23,
        fontFamily: 'lucida-grande'
    },
    title: {
        color: TEXT_DARK,
        fontSize: 16,
        lineHeight: 23,
        fontFamily: 'lucida-grande-bold',
        marginBottom: 3
    },
    flexRow: {
        flexDirection: 'row',
        alignItems: 'center',
        marginBottom: 10
    }
})