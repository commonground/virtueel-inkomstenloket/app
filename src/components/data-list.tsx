import React, { useState, useRef, useEffect, useContext } from "react";
import { Text, View, StyleSheet, Image, Animated } from 'react-native';
import { TouchableOpacity } from 'react-native-gesture-handler';

// Import styles and colors
import { getGenericStyles } from "../style/globalStyles";
import { TEXT_DARK } from "../style/colors";

// Import components
import { Chevron } from "./icons/chevron";

type DataListProps = {
    list: any;
    title: string;
    icon?: any;
    visibleLength: number;
}

const genericStyles = getGenericStyles();

export const DataList = (props: DataListProps) => {
    const maxLength = Object.keys(props.list).length;
    const animatedController = useRef(new Animated.Value(0)).current;
    const [open, setOpen] = useState(false);
    const [listLength, setListLength] = useState(props.visibleLength);
    const [text, setText] = useState(`Laat al uw ${props.title} zien (${maxLength} regels)`);

    const chevronRotation = animatedController.interpolate({
        inputRange: [0, 1],
        outputRange: ['270deg', '90deg']
    });

    useEffect(() => {
        if (!open) {
            setListLength(props.visibleLength);
            setText(`Laat al uw ${props.title} zien (${maxLength} regels)`);
            Animated.timing(animatedController, {
                duration: 300,
                toValue: 1,
                useNativeDriver: false
            }).start();
        } else {
            setListLength(maxLength);
            setText(`Verberg extra ${props.title}`);
            Animated.timing(animatedController, {
                duration: 300,
                toValue: 0,
                useNativeDriver: false
            }).start();
        }
    }, [open]);

    return (
        <View>
            <View style={{ flexDirection: 'row' }}>{props.icon && <Image style={styles.icon} source={props.icon}></Image>}<Text style={genericStyles.title}>Uw {props.title}</Text></View>
            <View style={styles.bottomSeparator}>
                {Object.keys(props.list).map((key: string, i: number) => {
                    if (i < listLength) {
                        return (
                            <View key={key} style={styles.topSeparator}>
                                <Text style={styles.subtitle}>{key}</Text>
                                <Text style={styles.text}>{props.list[key]}</Text>
                            </View>
                        )
                    }
                })}
            </View>
            <TouchableOpacity onPress={() => setOpen(!open)} style={styles.displayMoreButton}>
                <Animated.View style={[styles.chevron, { transform: [{ rotate: chevronRotation }] }]}>
                    <Chevron color={'#000'} />
                </Animated.View>
                <Text style={[styles.text, styles.buttonText]}>{text}</Text>
            </TouchableOpacity>
        </View>
    )
};

const styles = StyleSheet.create({
    displayMoreButton: {
        flexDirection: 'row',
        alignItems: 'center',
        marginBottom: 40,
        marginTop: 20
    },
    chevron: {
        width: 10,
        height: 15,
        marginRight: 10
    },
    subtitle: {
        fontFamily: 'lucida-grande-bold',
        marginTop: 10
    },
    text: {
        fontFamily: 'lucida-grande',
        marginVertical: 10
    },
    buttonText: {
        fontSize: 14
    },
    topSeparator: {
        borderTopColor: TEXT_DARK,
        borderTopWidth: 0.3,
    },
    bottomSeparator: {
        borderBottomColor: TEXT_DARK,
        borderBottomWidth: 0.3,

    },
    icon: {
        marginRight: 10
    }
});
