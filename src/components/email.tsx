// React(-native) imports
import React, { useState } from 'react';
import { StyleSheet, View, Text, TextInput, Dimensions } from 'react-native';

// Import generic styles
import { getGenericStyles } from '../style/globalStyles';

// Import text
import { emailTitle } from '../constants/content-constants'
import { TEXT_DARK, MEDIUM_RED } from '../style/colors';

const genericStyles = getGenericStyles();

const EMAILREGEX = /^([a-zA-Z0-9_\-\.+]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5})$|^Uw e-mailadres$|^$/; //

type EmailProps = { onEmailInput: (input: string) => void };

export function Email(props: EmailProps) {
    const [value, setValue] = useState('Uw e-mailadres');

    const handleInput = (input: string) => {
        setValue(input);
        props.onEmailInput(input);
    }

    return (
        <View style={styles.container}>
            <Text style={[genericStyles.text, { fontFamily: 'lucida-grande-bold' }]}>{emailTitle}</Text>
            <View style={styles.inputContainer}>
                <Text style={styles.optionalText}>(optioneel)</Text>
                <TextInput
                    style={[(EMAILREGEX.test(value) ? { borderColor: TEXT_DARK } : { borderColor: MEDIUM_RED }), styles.textInput]}
                    clearTextOnFocus={true}
                    autoCorrect={false}
                    autoCapitalize={'none'}
                    onChangeText={handleInput}
                    value={value}
                />
            </View>
        </View>
    )
}


const styles = StyleSheet.create({
    container: {
        marginTop: 10
    },
    inputContainer: {
        alignItems: 'flex-end'
    },
    textInput: {
        height: 40,
        alignSelf: 'stretch',
        color: '#646464',
        borderWidth: 1,
        padding: 10
    },
    optionalText: {
        color: '#3F3F3F',
        fontSize: 14,
        lineHeight: 23,
        fontFamily: 'lucida-grande'
    }
});