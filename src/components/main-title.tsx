// React(-native) imports
import React from 'react';
import { StyleSheet, View, Text, Platform } from 'react-native';



// Define types
export type MainTextProps = {
    title?: string,
    step?: string
}


export function MainTitle(props: MainTextProps) {
    return (
        <View style={styles.container}>
            {props.title && <Text style={styles.title}>{props.title}</Text>}
            {props.step && <Text style={styles.steps}>{props.step}</Text>}
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        marginHorizontal: 30,
        marginTop: 15
    },
    title: {
        fontSize: 24,
        lineHeight: 34,
        fontFamily: 'lucida-grande-bold',
        letterSpacing: 0.4
    },
    steps: {
        fontSize: 16,
        lineHeight: Platform.OS === 'ios' ? 23 : 25,
        marginTop: 10,
        fontFamily: 'lucida-grande',
    }
});