// React(-native) imports
import React from 'react';
import Svg, { Path } from "react-native-svg"

export function Check(props: { width?: number, height?: number, altFill?: string }) {
    return (
        <Svg
            width={props.width || 30}
            height={props.height || 30}
            viewBox="0 0 30 38"
            fill="none"
        >
            <Path
                fillRule="evenodd"
                clipRule="evenodd"
                d="M4.463 14.772a.969.969 0 00-1.393-.082L.74 16.816a1.032 1.032 0 00-.085 1.43l9.515 10.997a.972.972 0 001.393.083l17.7-16.143a1.032 1.032 0 00.084-1.43l-1.933-2.234a.969.969 0 00-1.393-.082L12.13 22.106a.969.969 0 01-1.393-.082l-6.274-7.252z"
                fill={props.altFill ?? "#32AB27"}
            />
        </Svg>
    )
}
