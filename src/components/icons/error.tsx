// React(-native) imports
import React from 'react';
import Svg, { Path } from "react-native-svg"
import { ERROR_RED } from '../../style/colors';

export function Error(props: { width?: number, height?: number, fill?: string }) {
    return (
        <Svg
            width={props.width || 30}
            height={props.height || 30}
            viewBox="0 0 30 30"
            fill="none"
        >
            <Path
                fillRule="evenodd"
                clipRule="evenodd"
                d="M18.8 15.055a.685.685 0 000-.969l-4.106-4.105a.685.685 0 010-.962l4.105-4.105a.685.685 0 000-.97l-.744-.743a.683.683 0 00-.969 0L12.98 7.306a.685.685 0 01-.961 0L7.914 3.2a.683.683 0 00-.97 0l-.743.744a.685.685 0 000 .969l4.105 4.105a.685.685 0 010 .962l-4.105 4.105a.685.685 0 000 .97l.744.743a.683.683 0 00.969 0l4.106-4.105a.685.685 0 01.961 0l4.105 4.105a.683.683 0 00.97 0l.743-.744z"
                fill={props.fill || "#C00"}
            />
        </Svg>
    )
}
