// React(-native) imports
import React from 'react';
import Svg, { Path } from "react-native-svg"

export function Chevron(props: { color?: string }) {
    return (
        <Svg
            width={10}
            height={16}
            viewBox="0 0 10 16"
            fill="none"
        >
            <Path
                d="M1.308 15.29a1 1 0 001.416 0l6.571-6.584a1 1 0 000-1.412L2.725.709a1 1 0 00-1.417 0l-.603.605a1 1 0 000 1.413l4.558 4.567a1 1 0 010 1.412L.705 13.273a1 1 0 000 1.413l.603.605z"
                fill={props.color || "#fff"}
            />
        </Svg>
    )
}

