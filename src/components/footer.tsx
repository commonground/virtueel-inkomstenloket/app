// React(-native) imports
import React from 'react';
import { StyleSheet, Text, View, TouchableOpacity, Image, Dimensions, FlatList } from 'react-native';
import * as Linking from 'expo-linking';

// Import colors
import { DARK_RED, MEDIUM_RED, LIGHT_RED, TEXT_LIGHT } from '../style/colors'

// Import text
import { footerText } from '../constants/content-constants'

const HEIGHT = 190;

// Define types:
type ItemProps = {
    title: string | null;
    text: string;
    url: string;
}

export function Footer() {
    const items: ItemProps[] = footerText;
    const goToUrl = (url: string) => {
        if (url) {
            Linking.openURL(url);
        } else {
            console.error('No toUrl prop in content constants.')
        }
    }

    const renderBackground = () => {
        return (
            <View style={styles.background}>
                <View style={styles.topSquare} />
                <View style={styles.bottomSquare} />
                <View style={styles.triangle} />
            </View>
        )
    };

    return (
        <View style={styles.container}>
            {renderBackground()}
            <Text style={styles.title}>Vragen of hulp nodig?</Text>

            {items.map((item, i) => {
                return (
                    <TouchableOpacity onPress={() => goToUrl(item.url)} style={styles.item} key={i}>
                        <Text>
                            <Text style={styles.itemTitle}>{item.title}</Text><Text style={styles.itemText}>{item.text}</Text>
                        </Text>
                    </TouchableOpacity>
                )
            })}

        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        height: HEIGHT,
        position: 'absolute',
        backgroundColor: MEDIUM_RED,
        bottom: 0
    },
    background: {
        position: 'absolute',
        top: 0
    },
    bottomSquare: {
        height: HEIGHT / 2,
        width: Dimensions.get('window').width,
        backgroundColor: LIGHT_RED,
    },
    topSquare: {
        height: HEIGHT / 2,
        width: Dimensions.get('window').width,
        backgroundColor: MEDIUM_RED
    },
    triangle: {
        position: 'absolute',
        top: 0,
        left: 0,
        width: 0,
        height: 0,
        backgroundColor: "transparent",
        borderStyle: "solid",
        borderRightWidth: HEIGHT + 20,
        borderBottomWidth: HEIGHT,
        borderRightColor: "transparent",
        borderBottomColor: DARK_RED,
    },
    title: {
        fontFamily: 'lucida-grande-bold',
        marginTop: HEIGHT / 5,
        marginLeft: 30,
        marginBottom: 5,
        color: TEXT_LIGHT,
        lineHeight: 18,
        fontSize: 18
    },
    item: {
        paddingVertical: 6,
        marginLeft: 30,
    },
    itemTitle: {
        fontFamily: 'lucida-grande-bold',
        color: TEXT_LIGHT,
        lineHeight: 16,
        fontSize: 16
    },
    itemText: {
        fontFamily: 'lucida-grande',
        textDecorationLine: "underline",
        lineHeight: 16,
        fontSize: 16,
        color: TEXT_LIGHT
    }

});
