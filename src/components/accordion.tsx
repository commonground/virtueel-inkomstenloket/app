import React, { useState, useRef, useEffect } from "react";
import { Text, View, StyleSheet, TouchableWithoutFeedback, Animated } from 'react-native';

// Import icon
import { AntDesign } from '@expo/vector-icons';

type AccordionListItemProps = {
    title: string;
    children: React.ReactNode;
}

export const AccordionListItem = (props: AccordionListItemProps) => {
    const [open, setOpen] = useState(true);
    const animatedController = useRef(new Animated.Value(0)).current;
    const [bodySectionHeight, setBodySectionHeight] = useState<number>(0);

    const bodyHeight = animatedController.interpolate({
        inputRange: [0, 1],
        outputRange: [0, bodySectionHeight!],
    });

    const arrowAngle = animatedController.interpolate({
        inputRange: [0, 1],
        outputRange: ['0rad', `${Math.PI}rad`],
    });

    useEffect(() => {
        if (open) {
            Animated.timing(animatedController, {
                duration: 300,
                toValue: 0,
                useNativeDriver: false
            }).start();
        } else {
            Animated.timing(animatedController, {
                duration: 300,
                toValue: 1,
                useNativeDriver: false
            }).start();
        }
    }, [open]);

    return (
        <View>
            <TouchableWithoutFeedback onPress={() => setOpen(!open)}>
                <View style={styles.titleContainer}>
                    <Animated.View style={{ transform: [{ rotateZ: arrowAngle }] }}>
                        <AntDesign name="down" size={20} color="black" />
                    </Animated.View>
                    <Text style={styles.text}>{props.title}</Text>
                </View>
            </TouchableWithoutFeedback>
            <Animated.View style={[styles.bodyBackground, { height: bodyHeight }]}>
                <View
                    style={styles.bodyContainer}
                    onLayout={event => {
                        setBodySectionHeight(event.nativeEvent.layout.height)
                    }
                    }>
                    {props.children}
                </View>
            </Animated.View>
        </View>
    );
};

const styles = StyleSheet.create({
    titleContainer: {
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'center',
        paddingVertical: 10,
        marginRight: 5
    },
    bodyBackground: {
        overflow: 'hidden',
    },
    bodyContainer: {
        padding: 15,
        position: 'absolute',
        bottom: 0
    },
    text: {
        flexShrink: 1,
        fontSize: 16,
        fontFamily: 'lucida-grande-bold',
        lineHeight: 23,
        marginLeft: 10
    }
});
