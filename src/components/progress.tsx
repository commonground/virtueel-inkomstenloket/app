// React(-native) imports
import React, { useState, useEffect, useRef } from 'react';
import { StyleSheet, View, Animated } from 'react-native';

// Import svg
import Svg, { Circle } from 'react-native-svg';
const AnimatedCircle = Animated.createAnimatedComponent(Circle);

// Import color
import Color from 'color';

// Import colors
import { DARK_BLUE } from '../style/colors'

// Define types
type CircularProgressProps = {
    progress?: number;
    size?: number;
    strokeWidth?: number;
    stroke?: string;
}


// Define constants
const DEFAULT_SIZE = 200;
const DEFAULT_STROKE_WIDTH = 12;
const DEFAULT_STROKE = DARK_BLUE;
const DEFAULT_BACKGROUND = '#ccc';

export function Circular(props: CircularProgressProps) {
    const [progress] = useState(new Animated.Value(Math.min(props.progress || 0, 100)))

    const progressRef = useRef<Animated.Value | null>(null); // To prevent running on first render
    useEffect(() => {
        if (progressRef.current) {
            progress.stopAnimation(() => {
                Animated.timing(progress, { toValue: Math.min(props.progress || 0, 100), duration: 500, useNativeDriver: true }).start();
            });
        } else {
            progressRef.current = progress;
        }
    }, [props.progress]);

    // Get values
    const size = props.size || DEFAULT_SIZE;
    const strokeWidth = props.strokeWidth || DEFAULT_STROKE_WIDTH;
    const stroke = new Color(props.stroke || DEFAULT_STROKE);
    const background = new Color(DEFAULT_BACKGROUND);

    // Calculate relevant values
    const radius = (size - strokeWidth) / 2;
    const circumfrence = radius * 2 * Math.PI;

    // Find stroke dash offset
    const interpolated = progress.interpolate({ inputRange: [0, 100], outputRange: [Math.PI * 2, 0] });
    const strokeDashOffset = Animated.multiply(interpolated, radius);

    return (
        <View style={styles['container']}>
            <Svg width={size} height={size}>
                <Circle
                    stroke={background.lighten(0).toString()}
                    fill={'none'}
                    cx={size / 2}
                    cy={size / 2}
                    r={radius}
                    strokeWidth={strokeWidth}
                />
                <AnimatedCircle
                    stroke={stroke.toString()}
                    fill={'none'}
                    cx={size / 2}
                    cy={size / 2}
                    r={radius}
                    strokeWidth={strokeWidth}
                    strokeDasharray={`${circumfrence} ${circumfrence}`}
                    strokeDashoffset={strokeDashOffset}
                    originX={size / 2}
                    originY={size / 2}
                    rotation={-90}
                />
            </Svg>
        </View>
    );

}

const styles = StyleSheet.create({
    container: {

    },
});
