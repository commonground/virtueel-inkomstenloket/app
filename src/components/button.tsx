// React(-native) imports
import React from 'react';
import { StyleSheet, View, Text, TouchableOpacity, StyleProp, ViewStyle } from 'react-native';

// Import color
import { DARK_BLUE, TEXT_LIGHT, SUCCESS_GREEN, ERROR_RED } from '../style/colors'

// Import components
import { Chevron } from './icons/chevron';
import { Check } from './icons/check';
import { Error } from './icons/error';

// Define types
export type ButtonProps = {
    label: string;
    active?: boolean;
    style?: StyleProp<ViewStyle>,
    purpose?: 'success' | 'error' | 'close'
    onPress?: () => void;
}


export function Button(props: ButtonProps) {
    // Get active
    const active = props.active !== false;
    let icon = <View style={styles.icon}><Chevron /></View>;
    let textColor = TEXT_LIGHT;
    let backgroundColor = DARK_BLUE;
    if (props.purpose === 'success') {
        icon = <View style={styles.icon}><Check width={19} height={19} altFill={TEXT_LIGHT} /></View>
        backgroundColor = SUCCESS_GREEN;
    } else if (props.purpose === 'error') {
        icon = <View style={styles.icon}><Error /></View>
        textColor = ERROR_RED;
        backgroundColor = TEXT_LIGHT;
    } else if (props.purpose === 'close') {
        icon = <View style={styles.icon}><Error fill={DARK_BLUE} /></View>
        textColor = DARK_BLUE;
        backgroundColor = TEXT_LIGHT;
    }

    const renderContent = (): JSX.Element => {
        return (
            <View style={styles.label}>
                {icon}
                <Text style={[styles.text, { color: textColor }]}>{props.label}</Text>
            </View>
        )
    }

    return (
        <View style={[styles.container, props.style]}>
            {
                active ?
                    <TouchableOpacity style={[styles.button, { backgroundColor: backgroundColor }, props.style]} onPress={props.onPress}>
                        {renderContent()}
                    </TouchableOpacity> :
                    <View style={[styles.button, props.style, { backgroundColor: backgroundColor, opacity: .25 }]}>
                        {renderContent()}
                    </View>
            }
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        justifyContent: 'center',
    },
    label: {
        flexDirection: 'row',
        justifyContent: 'center'
    },
    icon: {
        alignSelf: 'center',
        marginRight: 10,
        marginBottom: 3,
    },
    button: {
        width: '100%',
        paddingVertical: 10
    },
    text: {
        fontSize: 14,
        marginTop: 3,
        fontFamily: 'lucida-grande-bold',
    }
});