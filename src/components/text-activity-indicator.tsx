// React(-native) imports
import React, { useEffect, useState } from 'react';
import { StyleSheet, Text } from 'react-native';

// Declare constants
const WAIT_INTERVAL = 500;
const MAX_NUM_DOTS = 3;

export function TextActivityIndicator() {
    const [dotsShown, setDotsShown] = useState(0);

    useEffect(() => {
        const intervalHandle = setInterval(() => {
            setDotsShown(dotsShown < MAX_NUM_DOTS ? dotsShown + 1 : 0)
        }, WAIT_INTERVAL);

        return () => {
            clearInterval(intervalHandle);
        }
    }, [dotsShown]);

    return (
        <Text>
            <Text style={[dotsShown < 1 ? styles['hidden'] : null]}>.</Text>
            <Text style={[dotsShown < 2 ? styles['hidden'] : null]}>.</Text>
            <Text style={[dotsShown < 3 ? styles['hidden'] : null]}>.</Text>
        </Text>
    );
}

const styles = StyleSheet.create({
    hidden: {
        opacity: 0
    }
});