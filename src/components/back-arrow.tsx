// React(-native) imports
import React from 'react';
import { StyleSheet, View, Text, TouchableOpacity } from 'react-native';

// Import icons
import { AntDesign } from '@expo/vector-icons';
import { DARK_BLUE } from '../style/colors';

// Define types
export type BackArrowProps = {
    visible?: boolean;
    onPress?: () => void;
}

export function BackArrow(props: BackArrowProps) {
    return (
        <View>
            {props.visible &&
                <TouchableOpacity style={styles.container} onPress={props.onPress}>
                    <AntDesign name="arrowleft" size={24} color="black" />
                    <Text style={styles.text}>terug</Text>
                </TouchableOpacity>
            }
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginVertical: 15,
        marginLeft: 30,
        width: 80
    },
    text: {
        fontSize: 18,
        fontFamily: 'arial'
    }
});