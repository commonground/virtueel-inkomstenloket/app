// React(-native) imports
import React, { useState } from 'react';
import { StyleSheet, Platform, Text, View, Linking, TouchableOpacity, TouchableWithoutFeedback } from 'react-native';

// Import colors and styles
import { LIGHT_BLUE } from '../style/colors'
import { getGenericStyles } from '../style/globalStyles'

// Import text
import { faqText, explanationText } from '../constants/accordion-constants'
import { AccordionListItem } from './accordion';

type FaqProps = {
    page: string;
    type: 'explanation' | 'faq'
}

const genericStyles = getGenericStyles();

export function Dropdown(props: FaqProps): JSX.Element {
    const items = props.type === 'faq' ? faqText[props.page] : explanationText[props.page];

    const faqStartHeight = items ? 200 + (items.length * 30) : 200;
    const explanationStartHeight = ((items?.length || 3) * 10);
    const [height, setHeight] = useState(props.type === 'faq' ? faqStartHeight : explanationStartHeight);

    const setContainerHeight = (addedHeight: number) => {
        setHeight((props.type === 'faq' ? faqStartHeight : explanationStartHeight) + addedHeight);
    }

    const makeInteractive = (answer: any) => {
        let items: JSX.Element[] = [];
        answer.map((item: string | { url: string, text: string }, i: number) => {
            if (typeof item === 'string') {
                items.push(<Text key={i}>{item}</Text>)
            } else {
                items.push(<TouchableWithoutFeedback key={i} onPress={() => Linking.openURL(item.url)}><Text style={[genericStyles.link, styles.link]}>{item.text}</Text></TouchableWithoutFeedback>)
            }
        });
        return <Text key={answer[0]} style={styles.item}>{items}</Text>;
    }

    if (items) {
        return (
            <View style={[props.type === 'faq' && styles.container, { height: height }]}>
                {props.type === 'faq' && <Text style={styles.title}>Vraag en antwoord</Text>}
                <View style={props.type === 'faq' && styles.questionContainer} onLayout={event => setContainerHeight(event.nativeEvent.layout.height)}>
                    {items.map((item, i) => {
                        return (
                            <AccordionListItem title={item.question} key={i} >
                                {item.answer
                                    ? <Text style={styles.item}>{item.answer}</Text>
                                    : makeInteractive(item.interactiveAnswer)
                                }
                            </AccordionListItem>
                        )
                    })}
                </View>
            </View>
        )
    } else {
        return (
            <View style={[styles.emptyContainer, { height: height }]}></View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: LIGHT_BLUE,
        marginTop: 20
    },
    emptyContainer: {
        backgroundColor: '#fff'
    },
    questionContainer: {
        marginLeft: 30,
        marginRight: 10
    },
    title: {
        fontSize: 20,
        fontFamily: 'lucida-grande-bold',
        marginLeft: 15,
        marginVertical: 20,
    },
    item: {
        fontFamily: 'lucida-grande',
        fontSize: 16,
        lineHeight: Platform.OS === 'ios' ? 22 : 24,
    },
    link: {
        top: 2
    }

});
