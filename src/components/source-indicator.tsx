// React(-native) imports
import React from 'react';
import { StyleSheet, View, Text, Image } from 'react-native';

// Import react navigation
import { NavigationProp, NavigationState, ParamListBase } from '@react-navigation/native';

// Import constants
import { SOURCE_LOGO_MAP, SOURCE_TEXT_MAP, SourceLogoKeys } from '../constants/source-constants';

// Import components
import { Button } from '../components/button'
import { Check } from './icons/check';

// Define types
export type SourceIndicatorProps = {
    sourceId: SourceLogoKeys;
    active?: boolean;
    done?: boolean;
    navigation: NavigationProp<ParamListBase, string, NavigationState>;
}

export function SourceIndicator(props: SourceIndicatorProps) {
    const handleStartQuery = () => {
        props.navigation.navigate('query', { sourceId: props.sourceId });
    }

    return (
        <View style={[styles.container, (props.active && !props.done) && styles.containerActive]}>
            <View style={styles.introContainer}>
                <Image style={styles.logo} source={SOURCE_LOGO_MAP[(props.sourceId)]} />
                <Text style={styles.textContainer}>
                    <Text style={styles.title}>{`${SOURCE_TEXT_MAP[props.sourceId].title}\n`}</Text>
                    <Text>{SOURCE_TEXT_MAP[props.sourceId][(props.active && !props.done) ? 'intro' : 'info']}</Text>
                </Text>
            </View>
            <View style={styles.buttonContainer}>
                {props.done
                    ? <View style={styles.checkContainer}><Check width={45} height={45} /></View>
                    : <Button label="Inloggen" style={styles.button} onPress={handleStartQuery} active={props.active} />
                }
            </View>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: '#F2F2F2',
        alignItems: 'flex-end',
        marginVertical: 10,
        marginHorizontal: 20
    },
    containerActive: {
        height: 180
    },
    title: {
        fontFamily: 'lucida-grande-bold',
        fontSize: 20
    },
    introContainer: {
        padding: 10,
        flexDirection: 'row',
        justifyContent: 'space-around',
        flexGrow: 1
    },
    textContainer: {
        width: 240,
        fontSize: 16,
        fontFamily: 'lucida-grande',
        lineHeight: 22,
        marginRight: 10
    },
    logo: {
        width: 50,
        height: 50,
        marginRight: 20,
        borderRadius: 5
    },
    buttonContainer: {
        justifyContent: 'flex-end',
        alignItems: 'flex-end',
        paddingHorizontal: 10,
        marginBottom: 10
    },
    button: {
        width: 120,
    },
    checkContainer: {
        alignItems: 'center',
        paddingBottom: 10
    }

});