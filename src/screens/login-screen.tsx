// React(-native) imports
import React, { useRef, useState, useContext } from 'react';
import { StyleSheet, View, ScrollView, Dimensions, Text, TouchableOpacity, Platform } from 'react-native';
import { WebView } from 'react-native-webview';

// Import react navigation
import { NavigationProp, NavigationState, ParamListBase, RouteProp } from '@react-navigation/native';
import * as Linking from 'expo-linking';

//Import colors and styles
import { DARK_BLUE } from '../style/colors';
import { getGenericStyles } from '../style/globalStyles'

// Import Appstate
import { AppStateContext } from '../contexts/AppState';

// Import components
import { Logo } from '../components/logo';
import { Footer } from '../components/footer';
import { MainTitle } from '../components/main-title';
import { BackArrow } from '../components/back-arrow';
import { Dropdown } from '../components/dropdown';
import { Button } from '../components/button';

// Import text
import { loginContent, loginPartnerContent } from '../constants/content-constants'



// Define types
type LoginScreenProps = {
    navigation: NavigationProp<ParamListBase, string, NavigationState>;
    route: RouteProp<any, string>;
}

const genericStyles = getGenericStyles();

export function LoginScreen(props: LoginScreenProps) {
    const AppState = useContext(AppStateContext);
    const _scrollView = useRef<any>();
    const [screenHeight, setScreenHeight] = useState(0);
    const loginTextContent = AppState.isPartner ? loginPartnerContent : loginContent;

    const handleBack = () => {
        props.navigation.goBack();
    }

    const handleToUrl = (url: string) => {
        if (url) {
            Linking.openURL(url);
        } else {
            console.error('No toUrl prop in content constants.')
        }
    }

    const handleStart = () => {
        AppState.currentIndex = 0;
        props.navigation.navigate('overview');
    }

    const updateScrollPosition = () => {
        _scrollView.current.scrollTo({ x: 0, y: screenHeight, animated: false });
    }

    return (
        <ScrollView style={styles.container} contentContainerStyle={{ flexGrow: 1 }} ref={_scrollView} onContentSizeChange={updateScrollPosition} onMomentumScrollEnd={(e) => setScreenHeight(e.nativeEvent.contentOffset.y)} scrollEventThrottle={0}>
            <Logo />
            <View>
                <BackArrow visible={true} onPress={handleBack} />
                <MainTitle title={loginTextContent.title} step={loginTextContent.step} />
                <View>
                    {AppState.isPartner || <View style={{ height: 300 }}>
                        <WebView
                            style={styles.WebViewContainer}
                            // androidHardwareAccelerationDisabled={true}
                            javaScriptEnabled={true}
                            domStorageEnabled={true}
                            source={{ uri: 'https://www.youtube.com/embed/92nHDmD0LM0?modestbranding=1&playsinline=1&showinfo=0&rel=0' }}
                        />
                    </View>}
                    <View style={styles.contentContainer}>
                        {loginTextContent.content.map((content, i) => {
                            if (content.type === 'button') {
                                return (<Button key={i} label={content.text!} style={styles[content.type]} onPress={handleStart} />)
                            } else if (content.type === 'title') {
                                return (<Text key={i} style={genericStyles[content.type]}>{content.text}</Text>)
                            } else if (content.type === 'listItem') {
                                return (<Text key={i} style={[styles.rowText, genericStyles[content.type]]}>•   {content.text}</Text>)
                            } else if (content.type === 'touchableListItem') {
                                return (<TouchableOpacity key={i} style={styles.rowText} onPress={() => handleToUrl(content.toUrl!)}><Text style={genericStyles[content.type]}>•</Text><Text style={[genericStyles[content.type], styles.link]}>{content.text}</Text></TouchableOpacity>)
                            } else {
                                return (<Text key={i} style={genericStyles[content.type]}>{content.text}</Text>)
                            }
                        })}
                    </View>
                </View>
                <Dropdown page={props.route.name} type='faq' />
            </View>
            <Footer />
        </ScrollView>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        minHeight: Dimensions.get('window').height,
        backgroundColor: '#FFF',
    },
    image: {
        marginTop: 20,
        width: Dimensions.get('window').width,
        height: (Dimensions.get('window').width) * 0.65
    },
    button: {
        width: 130,
        justifyContent: 'flex-start',
        marginTop: 20,
        marginBottom: -10
    },
    contentContainer: {
        margin: 30
    },
    link: {
        textDecorationLine: 'underline',
        color: DARK_BLUE
    },
    rowText: {
        flexDirection: 'row'
    },
    WebViewContainer: {
        marginTop: Platform.OS === "android" ? 20 : 10
    },
});