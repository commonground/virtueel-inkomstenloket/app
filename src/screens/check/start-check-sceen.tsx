// React(-native) imports
import React from 'react';
import { StyleSheet, View, ScrollView, Dimensions, Text, Image, TouchableOpacity } from 'react-native';

// Import react navigation
import { NavigationProp, NavigationState, ParamListBase, RouteProp } from '@react-navigation/native';
import * as Linking from 'expo-linking';

//Import colors and styles
import { getGenericStyles } from '../../style/globalStyles'

// Import icons

// Import components
import { Logo } from '../../components/logo';
import { Footer } from '../../components/footer';
import { MainTitle } from '../../components/main-title';
import { BackArrow } from '../../components/back-arrow';
import { Button } from '../../components/button';

// Import text
import { startCheckContent } from '../../constants/content-constants'

// Define types
type StartCheckScreenProps = {
    navigation: NavigationProp<ParamListBase, string, NavigationState>;
    route: RouteProp<any, string>;
}

const genericStyles = getGenericStyles();

export function StartCheckScreen(props: StartCheckScreenProps) {
    const handleBack = () => {
        props.navigation.goBack();
    }

    const handleNext = () => {
        props.navigation.navigate('verifyData');
    }

    return (
        <ScrollView style={styles.container} contentContainerStyle={{ flexGrow: 1 }}>
            <Logo />
            <BackArrow visible={true} onPress={handleBack} />
            <MainTitle title={startCheckContent.title} step={startCheckContent.step} />
            <View style={styles.contentContainer}>
                {startCheckContent.content.map((content, i) => {
                    if (content.type === 'button') {
                        return (<Button key={i} label={content.text!} style={styles[content.type]} onPress={handleNext} />)
                    } else {
                        return (<Text key={i} style={genericStyles[content.type]}>{content.text}</Text>)
                    }
                })}
            </View>
            <View style={styles.footerContainer}></View>
            <Footer />
        </ScrollView>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        minHeight: Dimensions.get('window').height,
        backgroundColor: '#FFF',
    },
    button: {
        width: 130,
        justifyContent: 'flex-start',
        marginTop: 10,
    },
    contentContainer: {
        margin: 30,
    },
    footerContainer: {
        height: 200
    }
});