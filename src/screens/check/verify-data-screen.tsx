// React(-native) imports
import React, { useContext, useState, useRef } from 'react';
import { StyleSheet, View, ScrollView, Platform } from 'react-native';
import { WebView } from 'react-native-webview';

// Import react navigation
import { NavigationProp, NavigationState, ParamListBase, RouteProp } from '@react-navigation/native';

// Import components
import { Logo } from '../../components/logo';
import { Footer } from '../../components/footer';
import { MainTitle } from '../../components/main-title';
import { BackArrow } from '../../components/back-arrow';
import { Button } from '../../components/button';
import { DataList } from '../../components/data-list';

// Import text
import { verifyDataContent } from '../../constants/content-constants'

// Import appstate
import { AppStateContext } from '../../contexts/AppState';

// Import list generator / calculator function 
import { createDataLists } from '../../services/create-data-list'
import { calculateAmountToReceive } from '../../services/calculate-itt';

// Define types
type VerifyDataScreenProps = {
    navigation: NavigationProp<ParamListBase, string, NavigationState>;
    route: RouteProp<any, string>;
}

export function VerifyDataScreen(props: VerifyDataScreenProps) {
    const AppState = useContext(AppStateContext);
    const [screenHeight, setScreenHeight] = useState<number>();
    const _scrollView = useRef<any>();
    const dataLists = AppState.isPartner ? createDataLists(AppState.summaryPartner) : createDataLists(AppState.summary);
    const buttons = verifyDataContent.content.filter(content => content.type === 'button');

    const handleBack = () => {
        props.navigation.goBack();
    }

    const handleWrongData = () => {
        props.navigation.navigate('wrongData');
    }

    const handleStartCheck = () => {
        calculateAmountToReceive(dataLists).then((result) => {
            AppState.calcAmount = result;
            if (AppState.hasPartner) {
                props.navigation.navigate('checkResult');
            } else {
                props.navigation.navigate('startApplication');
            }

        }).catch(error => {
            console.log(error);
            props.navigation.navigate('wrongData');
        });
    }

    const updateScrollPosition = () => {
        _scrollView.current.scrollTo({ x: 0, y: screenHeight, animated: false });
    }

    return (
        <ScrollView style={styles.container} contentContainerStyle={{ flexGrow: 1, justifyContent: 'space-between' }} ref={_scrollView} onContentSizeChange={updateScrollPosition} onMomentumScrollEnd={(e) => setScreenHeight(e.nativeEvent.contentOffset.y)} scrollEventThrottle={0}>
            <Logo />
            <View style={{ marginBottom: 200 }}>
                <BackArrow visible={true} onPress={handleBack} />
                <MainTitle title={verifyDataContent.title} step={verifyDataContent.step} />
                <View>
                    <View style={{ height: 300 }}>
                        <WebView
                            style={styles.WebViewContainer}
                            // androidHardwareAccelerationDisabled={true}
                            javaScriptEnabled={true}
                            domStorageEnabled={true}
                            source={{ uri: 'https://www.youtube.com/embed/VMjaopaMMI4?modestbranding=1&playsinline=1&showinfo=0&rel=0' }}
                        />
                    </View>
                    <View style={styles.contentContainer}>
                        {dataLists[0] ? <DataList title={'gegevens'} icon={require('../../../assets/icons/gegevens.png')} list={dataLists[0]} visibleLength={3} /> : null}
                        {dataLists[1] ? <DataList title={'inkomen'} icon={require('../../../assets/icons/inkomen.png')} list={dataLists[1]} visibleLength={1} /> : null}
                        {dataLists[2] ? <DataList title={'eigen geld'} icon={require('../../../assets/icons/geld.png')} list={dataLists[2]} visibleLength={1} /> : null}
                        <View style={styles.buttonContainer}>
                            <Button label={buttons[0].text!} style={styles.button} purpose={'success'} onPress={handleStartCheck} />
                            <Button label={buttons[1].text!} style={styles.button} purpose={'error'} onPress={handleWrongData} />
                        </View>
                    </View>
                </View>
            </View>
            <Footer />
        </ScrollView>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#FFF',
    },
    buttonContainer: {
        flexDirection: 'row',
        justifyContent: 'space-evenly',
        marginLeft: 20
    },
    button: {
        width: 140,
        justifyContent: 'flex-start',
        marginTop: 10,
        marginRight: 50
    },
    contentContainer: {
        margin: 30
    },
    WebViewContainer: {
        marginTop: Platform.OS === "android" ? 20 : 10
    },
});