// React(-native) imports
import React, { useContext, useState } from 'react';
import { StyleSheet, View, ScrollView, Dimensions, Text, Platform, BackHandler } from 'react-native';

// Import react navigation
import { NavigationProp, NavigationState, ParamListBase, RouteProp } from '@react-navigation/native';

//Import colors and styles
import { BoxColors } from '../../style/colors';
import { getGenericStyles } from '../../style/globalStyles'

// Import components
import { Logo } from '../../components/logo';
import { Footer } from '../../components/footer';
import { MainTitle } from '../../components/main-title';
import { BackArrow } from '../../components/back-arrow';
import { Button } from '../../components/button';
import { TextBox } from '../../components/text-box';
import { Email } from '../../components/email';

// Import text
import { startApplicationContent, startApplicationContent_NegativeResult, startApplicationHeader } from '../../constants/content-constants'

// Import AppState
import { AppStateContext } from '../../contexts/AppState';
import WebView from 'react-native-webview';
import { sendPdf } from '../../services/pdf';

// Define types
type StartApplicationScreenProps = {
    navigation: NavigationProp<ParamListBase, string, NavigationState>;
    route: RouteProp<any, string>;
}

const genericStyles = getGenericStyles();

export function StartApplicationScreen(props: StartApplicationScreenProps) {
    const { summary, calcAmount, pdf } = useContext(AppStateContext);
    const textContent = !!parseFloat(calcAmount) ? startApplicationContent : startApplicationContent_NegativeResult;
    const button = textContent.content.find(content => content.type === 'button');
    const [emailAddress, setEmailAddress] = useState('');
    const [busy, setBusy] = useState(false);

    const expectedDate = new Date(+new Date + 1209600000); // Temp, set expected date for result in two weeks
    const userInfo = {
        name: `${summary?.persoonsgegevens?.aanvrager?.voornamen?.val} ${summary?.persoonsgegevens?.aanvrager?.achternaam?.val}`,
        date: `${expectedDate.getDate()}-${expectedDate.getMonth() + 1}-${expectedDate.getFullYear()}`,
        bankAccount: summary?.persoonsgegevens?.aanvrager?.bankrekeningnummer?.val || 'NL06 RABO 12345617',
        allowanceType: 'Individuele Inkomenstoeslag',
    }

    const handleBack = () => {
        props.navigation.goBack();
    }

    const handleNext = () => {
        setBusy(true);
        sendPdf(pdf, (summary?.persoonsgegevens?.aanvrager as any).bsn?.val).then(() => {
            console.log(`Todo: email kopie naar ${emailAddress} `);
            props.navigation.navigate('applicationSent');
            setBusy(false);
        }).catch(console.error);
    }

    const handleCloseApp = () => {
        console.log('closed app')
        BackHandler.exitApp();
    }

    return (
        <ScrollView style={styles.container} contentContainerStyle={{ flexGrow: 1 }}>
            <Logo />
            <View>
                <BackArrow visible={true} onPress={handleBack} />
                <MainTitle title={textContent.title} step={textContent.step} />
                <View>
                    <View style={{ height: 300 }}>
                        <WebView
                            style={styles.WebViewContainer}
                            // androidHardwareAccelerationDisabled={true}
                            javaScriptEnabled={true}
                            domStorageEnabled={true}
                            source={{ uri: 'https://www.youtube.com/embed/7UfxX6L7wLE?modestbranding=1&playsinline=1&showinfo=0&rel=0' }}
                        />
                    </View>
                    <View style={styles.contentContainer}>
                        <TextBox color={BoxColors.YELLOW} />
                        <Text style={genericStyles.text}>{startApplicationHeader}</Text>
                        <TextBox color={BoxColors.GREY} extraInfo={userInfo} />
                        {textContent.content.map((content, i) => {
                            if (content.type === 'text' || content.type === 'title') {
                                return (<View key={i} style={{ flexDirection: 'row' }}><Text style={[genericStyles[content.type], { marginRight: 10 }]}>•</Text><Text style={genericStyles[content.type]}>{content.text}</Text></View>)
                            }
                        })}
                        {!!parseFloat(calcAmount) && <Email onEmailInput={setEmailAddress} />}
                        {!!parseFloat(calcAmount)
                            ? <Button label={button!.text!} style={styles.button} active={!busy} onPress={handleNext} />
                            : <Button label={button!.text!} style={styles.exitButton} purpose={'close'} onPress={handleCloseApp} />
                        }
                    </View>
                </View>
            </View>
            <Footer />
        </ScrollView>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        minHeight: Dimensions.get('window').height,
        backgroundColor: '#FFF',
    },
    button: {
        width: 210,
        justifyContent: 'flex-start',
        marginTop: 20,
    },
    exitButton: {
        width: 140,
        justifyContent: 'flex-start',
        marginTop: 10,
    },
    contentContainer: {
        margin: 30,
        marginBottom: 230
    },
    WebViewContainer: {
        marginTop: Platform.OS === "android" ? 20 : 10
    },
});