// React(-native) imports
import React, { useContext } from 'react';
import { StyleSheet, View, ScrollView, Dimensions, Text, Image, TouchableOpacity, BackHandler } from 'react-native';

// Import react navigation
import { NavigationProp, NavigationState, ParamListBase, RouteProp } from '@react-navigation/native';

//Import colors and styles
import { BoxColors } from '../../style/colors';
import { getGenericStyles } from '../../style/globalStyles'

// Import components
import { Logo } from '../../components/logo';
import { Footer } from '../../components/footer';
import { MainTitle } from '../../components/main-title';
import { BackArrow } from '../../components/back-arrow';
import { Button } from '../../components/button';
import { TextBox } from '../../components/text-box';

// Import text
import { applicationSentContent, applicationSentHeader } from '../../constants/content-constants'

// Import AppState
import { AppStateContext } from '../../contexts/AppState';


// Define types
type ApplicationSentScreenProps = {
    navigation: NavigationProp<ParamListBase, string, NavigationState>;
    route: RouteProp<any, string>;
}

const genericStyles = getGenericStyles();

export function ApplicationSentScreen(props: ApplicationSentScreenProps) {
    const { summary } = useContext(AppStateContext);
    const userInfo = {
        name: `${summary?.persoonsgegevens?.aanvrager?.voornamen?.val} ${summary?.persoonsgegevens?.aanvrager?.achternaam?.val}`,
        date: '08 februari 2021',
        bankAccount: 'NL06 RABO 12345617',
        allowanceType: 'Individuele Inkomenstoeslag',
    }
    const handleBack = () => {
        props.navigation.goBack();
    }

    const handleCloseApp = () => {
        console.log('closed app')
        BackHandler.exitApp();
    }

    return (
        <ScrollView style={styles.container} contentContainerStyle={{ flexGrow: 1 }}>
            <Logo />
            <View>
                <BackArrow visible={true} onPress={handleBack} />
                <MainTitle title={applicationSentContent.title} step={applicationSentContent.step} />
                <View>
                    <View style={styles.contentContainer}>
                        <TextBox color={BoxColors.GREEN} />
                        <Text style={genericStyles.text}>{applicationSentHeader}</Text>
                        <TextBox color={BoxColors.GREY} extraInfo={userInfo} />
                        {applicationSentContent.content.map((content, i) => {
                            if (content.type === 'button') {
                                return (<Button key={i} label={content.text!} style={styles.button} purpose={'close'} onPress={handleCloseApp} />)
                            } else if (content.type === 'text') {
                                return (<Text key={i} style={genericStyles[content.type]}>{content.text}</Text>)
                            }
                        })}
                    </View>
                </View>
            </View>
            <Footer />
        </ScrollView>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        minHeight: Dimensions.get('window').height,
        backgroundColor: '#FFF',
    },
    button: {
        width: 130,
        justifyContent: 'flex-start',
    },
    contentContainer: {
        margin: 30,
        marginBottom: 230
    }
});