// React(-native) imports
import React from 'react';
import { StyleSheet, View, Text, BackHandler, Dimensions, TouchableOpacity, Linking, TouchableWithoutFeedback } from 'react-native';

// Import react navigation
import { NavigationProp, NavigationState, ParamListBase, RouteProp } from '@react-navigation/native';

// Import components
import { Logo } from '../../components/logo';
import { Footer } from '../../components/footer';
import { MainTitle } from '../../components/main-title';
import { BackArrow } from '../../components/back-arrow';
import { Button } from '../../components/button';

// Import text constants
import { wrongDataContent, ContentTypes } from '../../constants/content-constants'

// Import styles
import { getGenericStyles } from '../../style/globalStyles'

// Define types
type OverviewScreenProps = {
    navigation: NavigationProp<ParamListBase, string, NavigationState>;
    route: RouteProp<any, string>;
}

const genericStyles = getGenericStyles();

export function WrongDataScreen(props: OverviewScreenProps) {
    const handleBack = () => {
        if (props.navigation.canGoBack()) {
            props.navigation.goBack();
        }
    }

    const handleCloseApp = () => {
        console.log('closed app')
        BackHandler.exitApp();
    }

    const makeInteractive = (content: any) => {
        let items: JSX.Element[] = [];
        content.interactiveText.forEach((item: string | { url: string, text: string }, i: number) => {
            if (typeof item === 'string') {
                items.push(<Text key={i}>{item}</Text>)
            } else {
                items.push(<TouchableWithoutFeedback key={i} onPress={() => Linking.openURL(item.url)}><Text style={genericStyles.link}>{item.text}</Text></TouchableWithoutFeedback>)
            }
        });
        return <Text key={content.type} style={[genericStyles[content.type as ContentTypes.text]]}>• {items}</Text>;
    }

    return (
        <View style={styles.container}>
            <Logo />
            <View>
                <BackArrow visible={true} onPress={handleBack} />
                <MainTitle title={wrongDataContent.title} step={wrongDataContent.step} />
                <View style={styles.contentContainer}>
                    {wrongDataContent.content.map((content, i) => {
                        if (content.type === 'button') {
                            return (<Button key={i} label={content.text!} style={styles.button} purpose={'close'} onPress={handleCloseApp} />)
                        } else {
                            return (
                                content.text
                                    ? <Text key={i} style={genericStyles[content.type]}>{content.text}</Text>
                                    : makeInteractive(content)
                            )
                        }
                    })}
                </View>
            </View>
            <Footer />
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#FFF',
    },
    contentContainer: {
        marginHorizontal: 30,
        marginVertical: 20,
    },
    button: {
        width: 130,
        justifyContent: 'flex-start',
        marginTop: 5
    },

});