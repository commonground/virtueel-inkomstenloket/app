// React(-native) imports
import React, { useContext } from 'react';
import { StyleSheet, View, ScrollView, Dimensions, Text, Linking, TouchableWithoutFeedback } from 'react-native';

// Import react navigation
import { NavigationProp, NavigationState, ParamListBase, RouteProp } from '@react-navigation/native';

//Import colors and styles
import { BoxColors } from '../../style/colors';
import { getGenericStyles } from '../../style/globalStyles'

// Import components
import { Logo } from '../../components/logo';
import { Footer } from '../../components/footer';
import { MainTitle } from '../../components/main-title';
import { BackArrow } from '../../components/back-arrow';
import { Button } from '../../components/button';
import { Check } from '../../components/icons/check';

// Import AppState
import { AppStateContext } from '../../contexts/AppState'

// Import text
import { handBackContent, handToPartnerContent, ContentTypes } from '../../constants/content-constants'


// Define types
type CheckResultScreenProps = {
    navigation: NavigationProp<ParamListBase, string, NavigationState>;
    route: RouteProp<any, string>;
}

const genericStyles = getGenericStyles();

export function CheckResultScreen(props: CheckResultScreenProps) {
    const AppState = useContext(AppStateContext);
    const checkResultContent = AppState.hasPartner ? handToPartnerContent : handBackContent;
    const smallScreenMinHeight = (Dimensions.get('window').height < 720) ? Dimensions.get('window').height + 120 : Dimensions.get('window').height;

    const handleBack = () => {
        props.navigation.goBack();
    }

    const handleNext = () => {
        if (AppState.hasPartner) {
            AppState.setAppState({
                ...AppState, ...{
                    blobList: [],
                    session: null,
                    hasPartner: false,
                    isPartner: true
                }
            });
            props.navigation.navigate('login');
        } else {
            props.navigation.navigate('startApplication');
        }
    }

    const renderBox = () => {
        return (
            checkResultContent.content.filter(content => content.type === 'listItem').map((content, i) => {
                if (i === 0) {
                    return (
                        <View key={i} style={{ flexDirection: 'row', marginLeft: 5 }}>
                            <Check /><Text style={[genericStyles.listItem, { paddingTop: 5 }]}>{content.text}</Text>
                        </View>
                    )
                } else if (i === 1) {
                    return (<Text key={i} style={[genericStyles.listItem, { fontFamily: 'lucida-grande-bold' }]}>{content.text}</Text>)
                } else {
                    return (<Text key={i} style={genericStyles.listItem}>{content.text}</Text>)
                }
            })
        )
    }

    return (
        <ScrollView style={styles.container} contentContainerStyle={{ flexGrow: 1, minHeight: smallScreenMinHeight }}>
            <Logo />
            <View>
                <BackArrow visible={true} onPress={handleBack} />
                <MainTitle title={checkResultContent.title} step={checkResultContent.step} />
                <View>
                    <View style={styles.contentContainer}>
                        {renderBox()}
                    </View>
                    <View style={styles.buttonContainer}>
                        {checkResultContent.content.filter(content => content.type === 'button').map((content, i) => {
                            return (<Button key={'but' + i} label={content.text!} style={styles.button} onPress={handleNext} />)
                        })}
                    </View>
                </View>
            </View>
            <Footer />
        </ScrollView>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#FFF',
    },
    button: {
        width: 130,
        justifyContent: 'flex-start',
        marginTop: 5
    },
    contentContainer: {
        margin: 30,
        marginBottom: 30,
        paddingVertical: 15,
        paddingHorizontal: 10,
        backgroundColor: BoxColors.YELLOW
    },
    buttonContainer: {
        marginLeft: 30
    }
});