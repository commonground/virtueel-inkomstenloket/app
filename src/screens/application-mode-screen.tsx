// React(-native) imports
import React, { useRef, useState } from 'react';
import { StyleSheet, View, ScrollView, Dimensions, Text, Image, TouchableOpacity } from 'react-native';

// Import react navigation
import { NavigationProp, NavigationState, ParamListBase, RouteProp } from '@react-navigation/native';

//Import colors and styles
import { getGenericStyles } from '../style/globalStyles'

// Import components
import { Logo } from '../components/logo';
import { Footer } from '../components/footer';
import { MainTitle } from '../components/main-title';
import { BackArrow } from '../components/back-arrow';
import { Dropdown } from '../components/dropdown';

// Import text
import { applicationModeContent } from '../constants/content-constants'

// Define types
type ApplicationModeScreenProps = {
    navigation: NavigationProp<ParamListBase, string, NavigationState>;
    route: RouteProp<any, string>;
}

const genericStyles = getGenericStyles();

export function ApplicationModeScreen(props: ApplicationModeScreenProps) {
    const _scrollView = useRef<any>();
    const [screenHeight, setScreenHeight] = useState(0);

    const handleBack = () => {
        props.navigation.goBack();
    }

    const handleStart = () => {
        props.navigation.navigate('login');
    }

    const updateScrollPosition = () => {
        _scrollView.current.scrollTo({ x: 0, y: screenHeight, animated: false });
    }

    return (
        <ScrollView style={styles.container} contentContainerStyle={{ flexGrow: 1 }} ref={_scrollView} onContentSizeChange={updateScrollPosition} onMomentumScrollEnd={(e) => setScreenHeight(e.nativeEvent.contentOffset.y)} scrollEventThrottle={0}>
            <Logo />
            <View>
                <BackArrow visible={true} onPress={handleBack} />
                <MainTitle title={applicationModeContent.title} step={applicationModeContent.step} />
                <View>
                    <View style={styles.contentContainer}>
                        {applicationModeContent.content.map((content, i) => {
                            if (content.type === 'title') {
                                return (<Text key={i} style={genericStyles[content.type]}>{content.text}</Text>)
                            } else if (content.type === 'blueText') {
                                return (
                                    <TouchableOpacity key={i} style={styles.blueTextContainer} onPress={handleStart}>
                                        <Image style={styles.image} source={content.img!}></Image>
                                        <Text style={{ flexShrink: 1 }}>
                                            <Text style={[genericStyles[content.type], genericStyles.blueTextStyles]}>{content.text} </Text>
                                            {content.smallText && <Text style={genericStyles[content.type]}>{content.smallText}</Text>}
                                        </Text>
                                    </TouchableOpacity>
                                )
                            }
                        })}
                    </View>
                </View>
                <Dropdown page={props.route.name} type='faq' />
            </View>
            <Footer />
        </ScrollView>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        minHeight: Dimensions.get('window').height,
        backgroundColor: '#FFF',
    },
    image: {
        width: 100,
        //height: 126, // image height / (screenwidth / image width)
        marginRight: 10
    },
    blueTextContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        marginVertical: 25

    },
    contentContainer: {
        margin: 20
    }
});