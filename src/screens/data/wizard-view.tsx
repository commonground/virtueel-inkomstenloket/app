// React(-native) imports
import React, { useRef, useState } from 'react';
import { StyleSheet, View, Text, Image, ScrollView, Dimensions } from 'react-native';
import { NavigationProp, ParamListBase, NavigationState } from '@react-navigation/native';

// Import components
import { Circular } from '../../components/progress';
import { Logo } from '../../components/logo';
import { Footer } from '../../components/footer';
import { MainTitle } from '../../components/main-title';
import { Dropdown } from '../../components/dropdown';
import { Button } from '../../components/button';
import { Check } from '../../components/icons/check';
// Import styles
import { TEXT_DARK } from '../../style/colors';

// Import constants
import { SOURCE_TEXT_MAP } from '../../constants/source-constants';


// Define types
type WizardViewProps = {
    sourceId: string;
    progress: number;
    queryDone: boolean;
    navigation: NavigationProp<ParamListBase, string, NavigationState>;
};


export const WizardView = (props: WizardViewProps): JSX.Element => {
    const { sourceId, progress } = props;
    const _scrollView = useRef<any>();
    const [screenHeight, setScreenHeight] = useState(0);

    const handleBack = () => {
        props.navigation.navigate('overview');
    }

    const updateScrollPosition = () => {
        _scrollView.current.scrollTo({ x: 0, y: screenHeight, animated: false });
    }

    return (
        <ScrollView style={styles.container} contentContainerStyle={{ flexGrow: 1, justifyContent: 'space-between' }} ref={_scrollView} onContentSizeChange={updateScrollPosition} onMomentumScrollEnd={(e) => setScreenHeight(e.nativeEvent.contentOffset.y)} scrollEventThrottle={0}>
            <View>
                <Logo />
                <MainTitle title={`Gegevens ophalen bij ${SOURCE_TEXT_MAP[sourceId].title}`} step={'Stap 1 van 3'} />
            </View>
            <View style={styles.contentContainer}>
                <View style={styles.progressContainer}>
                    {props.queryDone ? <Check width={45} height={45} /> : <Circular progress={progress} size={60} />}
                    <Text style={styles.text}>{SOURCE_TEXT_MAP[sourceId].text}</Text>
                </View>
                <Button label={"Volgende"} style={styles.button} active={props.queryDone} onPress={handleBack} />
            </View>
            <View style={styles.faqContainer} >
                <Dropdown page={'wizard'} type='faq' />
            </View>
            <Footer />
        </ScrollView>
    )
}

const styles = StyleSheet.create({
    container: {
        position: 'absolute',
        top: 0, bottom: 0, left: 0, right: 0,
        backgroundColor: 'white',
    },
    progressContainer: {
        flexDirection: 'row',
        marginHorizontal: 30,
    },
    faqContainer: {
        paddingBottom: 30
    },
    contentContainer: {
        paddingVertical: 50
    },
    title: {
        fontSize: 19,
        color: TEXT_DARK,
        marginBottom: 5,
        textAlign: 'center'
    },
    text: {
        flex: 2,
        marginLeft: 20,
        marginRight: 40,
        fontSize: 16,
        lineHeight: 23,
        fontFamily: 'lucida-grande',
    },
    button: {
        width: 130,
        marginLeft: 10,
        marginTop: 25
    },
    paragraph: {
        fontSize: 17,
        color: TEXT_DARK
    },
    logo: {
        width: 125,
        height: 125,
        borderRadius: 30
    }
});