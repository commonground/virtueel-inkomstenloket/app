// React(-native) imports
import React, { useState, useEffect, useRef, MutableRefObject, useContext } from 'react';
import { StyleSheet, View, SafeAreaView, Text, ActivityIndicator, Linking, Alert } from 'react-native';

// Import react navigation
import { NavigationProp, NavigationState, ParamListBase, RouteProp } from '@react-navigation/native';

// Import brondata api rn component
import { IwizeBrondataView, QueryFailureType } from '@iwize-aqopi/iwize-brondata-rn-component';

// Import components and views
import { TextActivityIndicator } from '../../components/text-activity-indicator'
import { WizardView } from './wizard-view'

//import { AppState } from '../../../models/AppState';
import { AppStateContext } from '../../contexts/AppState'

// Import colors
import { TEXT_DARK } from '../../style/colors'

// Import constants
import { AuthState, configurationEndpointStub, configurationEndpoint, parserEndpoint, consumerHostname, ConfigIds } from '../../constants/aqopi-constants';
import { SOURCES_LENGTH, SOURCE_TEXT_MAP } from '../../constants/source-constants';
import { getPdf } from '../../services/pdf';

// Define types
type QueryScreenProps = {
    navigation: NavigationProp<ParamListBase, string, NavigationState>;
    route: RouteProp<any, string>
}

export type ConfigId = {
    id: string;
    data: string[];
}


// Declare constants
const MOCK_PROGRESS_INTERVAL = 750;
const DONE_DELAY = 750;


export function QueryScreen(props: QueryScreenProps) {
    let brondataView = useRef() as MutableRefObject<IwizeBrondataView>;
    const AppState = useContext(AppStateContext);
    const { sourceId } = props.route.params!;
    const currentConfig = ConfigIds.find(config => config.id === sourceId)!;
    const isLast = currentConfig.id === Object.keys(SOURCE_TEXT_MAP)[SOURCES_LENGTH - 1]

    // State hooks
    const [authState, setAuthState] = useState(AuthState.INITIAL);
    const [progress, setProgress] = useState(0);
    const [showWizard, setShowWizard] = useState(false);
    const [queryDone, setQueryDone] = useState(false);
    const [url, setUrl] = useState('');
    const [showActivityIndicator, setShowActivityIndicator] = useState(true);


    useEffect(() => {
        try {
            console.log(`Has session: ${!!AppState.session}`);

            if (!AppState.session) {
                // Init the session
                const session = brondataView.current.init({
                    configEndpoint: AppState.useStub ? configurationEndpointStub : configurationEndpoint,
                    parserEndpoint: parserEndpoint,
                    consumerHostname: consumerHostname
                });

                // Update app state
                AppState.session = session;

                setTimeout(() => { console.log('After delay', AppState.session) }, 2000);
            } else {
                // Assign session
                brondataView.current.setSession(AppState.session);
            }

            // Attach event handlers
            brondataView.current.on('progress', (progress) => setProgress(progress));
            brondataView.current.on('authenticate', () => setTimeout(() => {
                setShowActivityIndicator(false);
                setAuthState(AuthState.AUTHENTICATING);
            }, 200));

            brondataView.current.on('authenticated', () => {
                setQueryDone(false);
                setShowWizard(true);
                setAuthState(AuthState.NOT_AUTHENTICATED);
            });
            brondataView.current.on('load-page', (url) => setUrl(url));
            handleStart(currentConfig);

        } catch (err) {
            console.log('Query failure caught in useEffect', err.message)
        }

        return (() => console.log('end of session'))

    }, []);

    const handleStart = async (currentConfig: ConfigId) => {
        try {
            setAuthState(AuthState.NOT_AUTHENTICATED);

            // Perform query
            await brondataView.current.query(currentConfig.id, ...currentConfig.data);

            // Log parse result
            // console.log(JSON.stringify(result.parseResult.data.summary.data));
            // console.log(JSON.stringify(brondataView.current.blobs));

            AppState.blobList = AppState.blobList.concat(brondataView.current.blobs);

            if (isLast) {
                // Assign summary and pdf
                // Perform parsing
                setProgress(80);
                const result = await brondataView.current.parse();
                setProgress(90);
                const pdf = await getPdf(result);
                setProgress(100);

                if (AppState.isPartner) {
                    AppState.summaryPartner = result.parseResult.data.summary.data;
                    AppState.pdfPartner = pdf;
                } else {
                    AppState.summary = result.parseResult.data.summary.data;
                    AppState.pdf = pdf;
                }
            }

            AppState.currentIndex++;
            setQueryDone(true);

        } catch (err) {
            console.log('Query failure:' + err);
            handleQueryFailure(err);
        }
    }

    const goBack = () => {
        if (props.navigation.canGoBack()) {
            props.navigation.goBack();
        }
    }

    const handleQueryFailure = (error: { type: QueryFailureType, reasonCode: string }) => {
        console.log('Error', JSON.stringify(error))
        const alertProps = {
            title: 'Er is een fout opgetreden',
            message: 'Er is een onverwachte fout opgetreden.',
            buttons: [
                { text: 'Terug', onPress: () => goBack() }
            ]
        };

        if (error.type === QueryFailureType.Canceled) {
            if (error.reasonCode) {
                if (error.reasonCode === "MOH_NOT_ACTIVATED") {
                    alertProps.title = 'MijnOverheid voorwaarden nog niet geaccepteerd';
                    alertProps.message = 'Om uw gegevens bij MijnOverheid op te halen moet u eerst de voorwaarden van MijnOverheid accepteren.';
                    alertProps.buttons.push({
                        text: 'Accepteer', onPress: () => {
                            Linking.openURL('https://mijn.overheid.nl/');
                            goBack();
                        }
                    });
                }
            } else {
                alertProps.message = 'U heeft het proces geannuleerd. Ga terug en probeer opnieuw.';
            }
        } else if (error.type === QueryFailureType.UserInconsistency) {
            alertProps.title = 'Er is een fout opgetreden';
            alertProps.message = 'U heeft ingelogd met het verkeerde DigiD account.';
        }

        Alert.alert(
            alertProps.title,
            alertProps.message,
            alertProps.buttons,
            { cancelable: false }
        );
    }

    const renderWizard = (): JSX.Element => {
        return (
            <WizardView
                sourceId={sourceId}
                progress={progress}
                queryDone={queryDone}
                navigation={props.navigation}
            />
        )
    }

    return (
        <SafeAreaView style={{ flex: 1 }}>
            <IwizeBrondataView ref={brondataView} />
            {showActivityIndicator ?
                <View style={[styles.activityIndicator]}>
                    <ActivityIndicator size="large" color={"#000"} />
                    <Text style={[styles['paragraph'], styles['status-text']]}>
                        Bezig<TextActivityIndicator />
                    </Text>
                </View>
                : null}
            {showWizard ? renderWizard() : null}
        </SafeAreaView>)
}

const styles = StyleSheet.create({
    container: {
        position: 'absolute',
        top: 0, bottom: 0, left: 0, right: 0,
        backgroundColor: 'white',
        paddingHorizontal: 20,
        paddingVertical: 20
    },

    activityIndicator: {
        position: 'absolute',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        top: 0, bottom: 0, left: 0, right: 0,
        backgroundColor: 'white',
    },
    'progress-anchor': {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    },
    'progress-container': {
        position: 'relative'
    },

    title: {
        fontSize: 19,
        color: TEXT_DARK,
        marginBottom: 5
    },

    paragraph: {
        fontSize: 17,
        color: TEXT_DARK
    },

    'logo-container': {
        justifyContent: 'center',
        alignItems: 'center'
    },

    logo: {
        width: 125,
        height: 125,
        borderRadius: 30
    },

    'status-text': {
        marginTop: 20
    }
});