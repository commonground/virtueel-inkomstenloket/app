// React(-native) imports
import React, { useState, useEffect, useContext, useRef } from 'react';
import { StyleSheet, View, ScrollView, Text } from 'react-native';

// Import react navigation
import { NavigationProp, NavigationState, ParamListBase, RouteProp } from '@react-navigation/native';

// Import AppState context
import { AppStateContext } from '../../contexts/AppState';

//Import colors and styles
import { getGenericStyles } from '../../style/globalStyles'

// Import components
import { SourceIndicator } from '../../components/source-indicator';
import { Logo } from '../../components/logo';
import { Footer } from '../../components/footer';
import { MainTitle } from '../../components/main-title';
import { BackArrow } from '../../components/back-arrow';
import { Dropdown } from '../../components/dropdown';
import { Button } from '../../components/button';
import { retrieveDataContent } from '../../constants/content-constants';
import { SOURCES_LENGTH, SOURCE_TEXT_MAP } from '../../constants/source-constants';


// Define types
type OverviewScreenProps = {
    navigation: NavigationProp<ParamListBase, string, NavigationState>;
    route: RouteProp<any, string>;
}

const genericStyles = getGenericStyles();

export function OverviewScreen(props: OverviewScreenProps) {
    const AppState = useContext(AppStateContext);
    // const [sourcesDone, setSourcesDone] = useState([false, false, false, false, false]);
    const [sourcesDone, setSourcesDone] = useState([false, false, false, false, false, false]);
    const _scrollView = useRef<any>();
    const [screenHeight, setScreenHeight] = useState(0);
    const [title, setTitle] = useState('Log in bij MijnOverheid');
    const step = retrieveDataContent.step;

    useEffect(() => {
        props.navigation.addListener('focus', handleGainFocus);
        return () => {
            props.navigation.removeListener('focus', handleGainFocus);
        }
    }, []);

    const updateScrollPosition = () => {
        _scrollView.current.scrollTo({ x: 0, y: screenHeight, animated: false });
    }

    const handleGainFocus = () => {
        const finishedMoh = AppState.currentIndex > 0;
        const finishedMbd = AppState.currentIndex > 1;
        const finishedUwv = AppState.currentIndex > 2;
        const finishedMpo = AppState.currentIndex > 3;
        const finishedMts = AppState.currentIndex > 4;
        const finishedDuo = AppState.currentIndex > 5;

        if (finishedDuo) {
            setTitle('Gegevens opgehaald')
        } else {
            setTitle(`Log in bij ${Object.values(SOURCE_TEXT_MAP)[AppState.currentIndex].title}`)
        }

        // setSourcesDone([finishedMoh, finishedMbd, finishedUwv, finishedMpo, finishedMts]);
        setSourcesDone([finishedMoh, finishedMbd, finishedUwv, finishedMpo, finishedMts, finishedDuo]);
    }

    const handleBack = () => {
        if (props.navigation.canGoBack()) {
            props.navigation.goBack();
        }
    }

    const handleNext = () => {
        props.navigation.navigate('verifyData');
    }

    return (
        <ScrollView style={styles.container} contentContainerStyle={{ flexGrow: 1, justifyContent: 'space-between' }} ref={_scrollView} onContentSizeChange={updateScrollPosition} onMomentumScrollEnd={(e) => setScreenHeight(e.nativeEvent.contentOffset.y)} scrollEventThrottle={0}>
            <Logo />
            <View>
                <BackArrow visible={true} onPress={handleBack} />
                <MainTitle title={title} step={step} />
                <View style={{ marginTop: 10 }}>
                    <SourceIndicator sourceId="moh-mb" active={true} done={sourcesDone[0]} navigation={props.navigation} />
                    <SourceIndicator sourceId="mbd-mb" active={sourcesDone[0]} done={sourcesDone[1]} navigation={props.navigation} />
                    <SourceIndicator sourceId="uwv-mb" active={sourcesDone[1]} done={sourcesDone[2]} navigation={props.navigation} />
                    <SourceIndicator sourceId="mpo-mb" active={sourcesDone[2]} done={sourcesDone[3]} navigation={props.navigation} />
                    <SourceIndicator sourceId="mts-mb" active={sourcesDone[3]} done={sourcesDone[4]} navigation={props.navigation} />
                    <SourceIndicator sourceId="duo-mb" active={sourcesDone[4]} done={sourcesDone[5]} navigation={props.navigation} />
                </View>
                {(sourcesDone.filter(Boolean)).length === SOURCES_LENGTH &&
                    retrieveDataContent.content!.map((content, i) => {
                        if (content.type === 'button') {
                            return (<Button key={i} label={content.text!} style={styles[content.type]} onPress={handleNext} />)
                        } else {
                            return (<Text key={i} style={[genericStyles[content.type], styles.text]}>{content.text}</Text>)
                        }
                    })
                }
                <Dropdown page={props.route.name} type='faq' />
            </View>
            <Footer />
        </ScrollView>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#FFF',
    },
    button: {
        width: 220,
        justifyContent: 'flex-start',
        marginVertical: 15,
        marginLeft: 10

    },
    text: {
        marginTop: 20,
        marginLeft: 20,
        marginRight: 30
    }

});
