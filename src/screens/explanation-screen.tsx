// React(-native) imports
import React from 'react';
import { StyleSheet, View, ScrollView, Dimensions, Platform } from 'react-native';
import { WebView } from 'react-native-webview';

// Import react navigation
import { NavigationProp, NavigationState, ParamListBase, RouteProp } from '@react-navigation/native';

// Import components
import { Logo } from '../components/logo';
import { Footer } from '../components/footer';
import { MainTitle } from '../components/main-title';
import { Dropdown } from '../components/dropdown';
import { Button } from '../components/button';

// Import text
import { explanationContent } from '../constants/content-constants'
import { BackArrow } from '../components/back-arrow';

// Define types
type ExplanationScreenProps = {
    navigation: NavigationProp<ParamListBase, string, NavigationState>;
    route: RouteProp<any, string>;
}


export function ExplanationScreen(props: ExplanationScreenProps) {
    const routeName = props.route.name;
    const isAppExplanation = routeName === 'app-explanation';
    const handlePress = () => {
        if (isAppExplanation) {
            props.navigation.navigate('iit-explanation');
        } else {
            props.navigation.navigate('application-mode');
        }
    }

    const handleBack = () => {
        props.navigation.goBack();
    }

    return (
        <ScrollView style={styles.container} contentContainerStyle={{ flexGrow: 1 }}>
            <Logo />
            <View>
                <BackArrow visible={true} onPress={handleBack} />
                <MainTitle title={explanationContent.title} />
                <View>
                    <View style={{ height: 300 }}>
                        <WebView
                            style={styles.WebViewContainer}
                            // androidHardwareAccelerationDisabled={true}
                            javaScriptEnabled={true}
                            domStorageEnabled={true}
                            source={{ uri: `https://www.youtube.com/embed/${isAppExplanation ? '5ACBtxZ6fMU' : '_eqGiAvx8nY'}?modestbranding=1&playsinline=1&showinfo=0&rel=0` }}
                        />
                    </View>
                    <View style={styles.contentContainer}>
                        <Dropdown page={routeName} type='explanation' />
                        {explanationContent.content.map((content, i) => {
                            if (content.type === 'button') {
                                return (<Button key={i} label={content.text!} style={styles[content.type]} onPress={handlePress} />)
                            }
                        })}
                    </View>
                </View>
            </View>
            <Footer />
        </ScrollView>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        minHeight: Dimensions.get('window').height,
        backgroundColor: '#FFF',
    },
    WebViewContainer: {
        marginTop: Platform.OS === "android" ? 20 : 10
    },
    button: {
        width: 170,
        justifyContent: 'flex-start',
        marginBottom: 100
    },
    contentContainer: {
        marginHorizontal: 20,
        marginVertical: 30
    }
});