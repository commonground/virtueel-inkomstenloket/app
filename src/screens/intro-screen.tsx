// React(-native) imports
import React, { useState, useContext } from 'react';
import { StyleSheet, View, ScrollView, Image, Dimensions, Text, TouchableOpacity, Pressable } from 'react-native';
import { Ionicons } from '@expo/vector-icons';

// Import react navigation
import { NavigationProp, NavigationState, ParamListBase, RouteProp } from '@react-navigation/native';


//Import colors and styles
import { TEXT_DARK, DARK_BLUE } from '../style/colors';
import { getGenericStyles } from '../style/globalStyles'

// Import icons
import { Feather } from '@expo/vector-icons';

// Import components
import { Logo } from '../components/logo';
import { Footer } from '../components/footer';
import { MainTitle } from '../components/main-title';
import { Button } from '../components/button';

// Import appstate
import { AppStateContext } from '../contexts/AppState';

// Import text
import { introContent } from '../constants/content-constants'

// Define types
type IntroScreenProps = {
    navigation: NavigationProp<ParamListBase, string, NavigationState>;
    route: RouteProp<any, string>;
}

const genericStyles = getGenericStyles();

export function IntroScreen(props: IntroScreenProps) {
    const [isSelected, setSelection] = useState(false);
    const AppState = useContext(AppStateContext);

    const handleStart = () => {
        props.navigation.navigate('application-mode');
    }
    const handleExplain = () => {
        props.navigation.navigate('app-explanation');
    }

    const handlePartnerChoice = () => {
        setSelection(!isSelected);
        AppState.hasPartner = !isSelected;

    }

    return (
        <ScrollView style={styles.container} contentContainerStyle={{ flexGrow: 1 }}>
            <Logo />
            <View>
                <MainTitle title={introContent.title} />
                <View>
                    {introContent.img && <Image
                        style={styles.image}
                        source={introContent.img!}

                    />}
                    <View style={styles.contentContainer}>

                        {introContent.content.map((content, i) => {
                            if (content.type === 'blueText') {
                                return (<TouchableOpacity key={i} style={genericStyles[content.type]} onPress={handleStart} ><Feather name={"chevron-right"} size={18} color={TEXT_DARK} /><Text style={genericStyles.blueTextStyles}>{content.text}</Text></TouchableOpacity>)
                            } else if (content.type === 'button') {
                                return (<Button key={i} label={content.text!} style={styles[content.type]} onPress={handleExplain} />)
                            } else {
                                return (<Text key={i} style={genericStyles[content.type]}>{content.text}</Text>)
                            }
                        })}
                        <View style={styles.checkboxContainer}>
                            <Pressable
                                style={[styles.checkboxBase, isSelected && styles.checkboxChecked]}
                                onPress={handlePartnerChoice}>
                                {isSelected && <Ionicons name="checkmark" size={20} color="white" />}
                            </Pressable>
                            <Text style={[genericStyles.text, { marginBottom: 120, marginLeft: 10 }]}>Ook voor mijn partner controleren</Text>
                        </View>
                    </View>
                </View>
            </View>
            <Footer />
        </ScrollView>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        minHeight: Dimensions.get('window').height,
        backgroundColor: '#FFF',
    },
    image: {
        marginTop: 20,
        width: Dimensions.get('window').width,
        height: 614 * (Dimensions.get('window').width / 792) // image height / (screenwidth / image width)
    },
    button: {
        width: 170,
        justifyContent: 'flex-start',
        marginTop: 5,
        marginBottom: 25
    },
    contentContainer: {
        margin: 30,
        marginBottom: 110
    },
    checkboxBase: {
        width: 24,
        height: 24,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 4,
        borderWidth: 2,
        borderColor: DARK_BLUE,
        backgroundColor: 'transparent',
        marginBottom: 120
    },
    checkboxChecked: {
        backgroundColor: DARK_BLUE,
    },
    checkboxContainer: {
        flexDirection: 'row',
        alignItems: 'center',
    },
});