
const BASE = 'https://summary.data.iwize.nl';
const SEND = 'https://zaken.service.iwize.nl/create';

export const getPdf = async (data: any) => {
    const formData = new FormData();
    formData.append('parseResult', JSON.stringify(data.parseResult));
    formData.append('application', 'vng');

    const pdfResponse = await fetch(BASE + '/generate-pdf', {
        method: 'POST',
        body: formData,
    });

    const pdf = await pdfResponse.text();

    return pdf;
}

export const sendPdf = async (pdf?: string, bsn?: string) => {
    if (!pdf) {
        return;
    }

    const idNum = Math.floor(Date.now() / 1000);
    await fetch(SEND, {
        method: 'POST',
        body: JSON.stringify({
            id: `ZAAL-VIL-${idNum}`,
            pdf,
            bsn: bsn || '555555021'
        }),
    });

    console.log(`ZAAL-VIL-${idNum}`);
}