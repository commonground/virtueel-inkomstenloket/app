import { IIT_BRM, IIT_URL } from '../constants/iit-constants'
// import * as XMLParser from 'react-xml-parser';

let XMLParser = require('react-xml-parser');

const createSOAPRequest = (lists: any) => {
   const age = getAgeFromBirthDate(lists[0].Geboortedatum);
   const brm = IIT_BRM;
   const woonplaats = lists[0].Woonplaats;
   const belastingjaar = new Date().getFullYear().toString();
   const aowLeeftijdBehaald = age > 67;
   const ouderDan21 = age > 21;
   const thuiswonendeKinderen = !!lists[0]['Aantal kinderen'];
   const alleenstaand = !!(lists[0]['Burgerlijke staat'] === 'Alleenstaand');
   const inkomenPerMaand = Math.floor((Object.values(lists[1])[0] as number) / 12);
   const vermogen = Math.floor((Object.values(lists[2])[0] as number));

   return `<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:brm="${brm}">
   <soapenv:Header/>
   <soapenv:Body>
      <brm:berekenIit>
         <rsiitMsg>
            <request belastingjaar="${belastingjaar}" berichtId="ID">
               <invoer>
                  <woonplaats>${woonplaats}</woonplaats>
                  <aowLeeftijdBehaald>${aowLeeftijdBehaald}</aowLeeftijdBehaald>
                  <ouderDan21>${ouderDan21}</ouderDan21>
                  <alleenstaand>${alleenstaand}</alleenstaand>
                  <thuiswonendeKinderen>${thuiswonendeKinderen}</thuiswonendeKinderen>
                  <inkomenPerMaand>${inkomenPerMaand}</inkomenPerMaand>
                  <vermogen>${vermogen}</vermogen>
               </invoer>
            </request>
            <response>
               <serviceResultaat>
                  <resultaatcode>?</resultaatcode>
                  <resultaatmelding>?</resultaatmelding>
                  <rulesversie>?</rulesversie>
                  <serviceversie>?</serviceversie>
               </serviceResultaat>
               <besluit>
                  <rechtBeschrijving>?</rechtBeschrijving>
                  <uitTeKerenToeslagBedrag>?</uitTeKerenToeslagBedrag>
               </besluit>
            </response>
         </rsiitMsg>
      </brm:berekenIit>
   </soapenv:Body>
</soapenv:Envelope>`
}

const getAgeFromBirthDate = (birthDateString: string) => {
   const splitBirthDate = (birthDateString).split("-").map(bdElement => Number(bdElement));
   const birthDate = new Date(splitBirthDate[2], splitBirthDate[1] - 1, splitBirthDate[0]);

   const today = new Date();
   let thisYear = 0;
   if (today.getMonth() < birthDate.getMonth() || (today.getMonth() == birthDate.getMonth()) && today.getDate() < birthDate.getDate()) {
      thisYear = 1;
   }
   const age = today.getFullYear() - birthDate.getFullYear() - thisYear;
   return age;
}

export const calculateAmountToReceive = async (lists: any): Promise<string> => {
   const body = createSOAPRequest(lists);

   console.log(body);

   const response = await fetch(IIT_URL,
      {
         method: 'POST',
         headers: {
            'Content-Type': 'text/xml'
         },
         body: body
      });

   if (!response.ok) {
      throw new Error(response.statusText);
   }

   const result = await response.text();
   console.log('RESULT\n', result);
   const parsedXML = new XMLParser().parseFromString(result);
   const amount = Number(parsedXML.getElementsByTagName('uitTeKerenToeslagBedrag')[0].value).toFixed(2).replace(/\./g, ',');


   console.log(parsedXML.getElementsByTagName('rechtBeschrijving')[0].value);
   console.log(parsedXML.getElementsByTagName('uitTeKerenToeslagBedrag')[0].value);
   return amount;
}

