type MaritalStatusses = 'gehuwd' | 'geregistreerdPartnerschap' | 'indSamenwonendEnFiscaalPartnerschap';
type MaritalStatusList = Record<MaritalStatusses, string>
type Incomes = { jaar: string; bedrag: number | null; };

export const createDataLists = (summary: any) => {
    const personalData = {
        'Voornaam': summary.persoonsgegevens?.aanvrager?.voornamen?.val,
        'Achternaam': summary.persoonsgegevens?.aanvrager?.achternaam?.val,
        'Geboortedatum': summary.persoonsgegevens?.aanvrager?.geboortedatum?.val,
        'Geboorteplaats': summary.persoonsgegevens?.aanvrager?.geboorteplaats?.val,
        'Geboorteland': summary.persoonsgegevens?.aanvrager?.geboorteland?.val,
        'Nationaliteit': summary.persoonsgegevens?.aanvrager?.nationaliteit?.val,
        'Burgerlijke staat': getMaritalStatus(summary.persoonsgegevens?.aanvrager?.burgerlijkeStaat?.val),
        'Aantal kinderen': summary.persoonsgegevens?.kinderen?.val.length || 0,
        ...getAdres(summary.persoonsgegevens?.adressen?.val)
    }

    const incomeData = () => {

        if (!summary.inkomens?.verzamelinkomens) {
            return {
                'Verzamelinkomen 2020': 0,
                'Verzamelinkomen 2019': 0,
                'Verzamelinkomen 2018': 0
            }
        }

        const incomes: Record<string, number | null> = {};
        summary.inkomens.verzamelinkomens.val.sort((a: Incomes, b: Incomes) => (a.jaar < b.jaar) ? 1 : -1).forEach((income: Incomes) => {
            incomes[`Verzamelinkomen ${income.jaar}`] = income.bedrag || 0;
        });

        return incomes;
    };

    const moneyData = {
        'Totale hoeveelheid geld op rekeningen': summary.bezittingen?.bankrekeningen ? formatNumber(summary.bezittingen.bankrekeningen.val.reduce((acc: number, current: any) => acc + current.waarde, 0)) : 0
    };

    const lists = [personalData, incomeData(), moneyData]
    return lists;
}

const getMaritalStatus = (maritalStatus: any) => {
    if (!maritalStatus)
        return 'Alleenstaand';

    const statusMap: MaritalStatusList = {
        gehuwd: 'Gehuwd',
        geregistreerdPartnerschap: 'Geregistreerd partnerschap',
        indSamenwonendEnFiscaalPartnerschap: 'Samenwonend fiscaal partnerschap'
    }
    const currentStatus = Object.keys(maritalStatus).filter(val => maritalStatus[val]) as MaritalStatusses[];
    return currentStatus?.length ? statusMap[currentStatus[0]] : 'Alleenstaand';
}

const getAdres = (addresses: any) => {
    if (!addresses)
        return {
            'Straat': null,
            'Huisnummer': null,
            'Postcode': null,
            'Woonplaats': null
        }
    const current = addresses.find((address: any) => address.huidig === true);
    return {
        'Straat': current.straat,
        'Huisnummer': current.huisnummer + (current.huisnummerToevoeging || ''),
        'Postcode': current.postcode,
        'Woonplaats': current.woonplaatsnaam
    }
}

function formatNumber(num: number) {
    return num.toString().replace(/\./g, ',').replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.')
}

