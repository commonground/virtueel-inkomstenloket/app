
type AccordionTextValues = { question: string, answer?: string, interactiveAnswer?: (string | { url: string, text: string })[] };
type AccordionText = Record<string, AccordionTextValues[]>;


const formattedAnswer1 =
    `Om te bepalen wie je bent halen we de volgende informatie op:

  •  Voornamen
  •  Tussenvoegsel
  •  Achternaam
  •  Geboortedatum
  •  Geboorteplaats
  •  Geboorteland
  •  Nationaliteit

De hoogte van de individuele inkomstentoeslag is onder andere afhankelijk van je leefsituatie, of je kinderen hebt, waar je woont, hoeveel vermogen je hebt en hoeveel je de afgelopen drie jaar hebt verdiend. Daarom halen we de volgende gegevens op:
    
  •  Burgerlijke staat
  •  Aantal kinderen
  •  Adres (straat, huisnummer, 
      postcode, woonplaats)
  •  Inkomen van de afgelopen drie jaar
  •  Geld op je rekeningen
    `

export const faqText: AccordionText = {
    'overview': [
        {
            'question': 'Welke gegevens worden opgehaald en waarom?',
            'answer': formattedAnswer1
        },
        {
            'question': 'Waarom moet ik twee keer inloggen?',
            'answer': 'Helaas is het technisch nog niet mogelijk om in één keer al je gegevens op te halen. Het is daarom nodig om per organisatie een keer in te loggen.'
        }
    ],
    'login': [
        {
            'question': 'Waarom moet ik inloggen met DigiD?',
            'answer': 'Om te kijken of je recht hebt op extra geld hebben we bepaalde gegevens van je nodig. Bijvoorbeeld waar je woont, of je kinderen hebt en hoe hoog je inkomen is. Om het voor jou zo gemakkelijk mogelijk te maken vragen we jou in te loggen met je DigiD.De data wordt dan automatisch opgehaald en jij hoeft niet allemaal formulieren op te sturen.Wel zo handig.'
        },
        {
            'question': 'Hoe zit het met mijn privacy?',
            'answer': 'De gegevens die wij ophalen staan tijdelijk op jouw telefoon. We slaan niks van je op. Niemand, behalve jijzelf, kan meekijken naar de gegevens die op worden gehaald.'
        },
        {
            'question': 'Ik wil eerst een check doen zonder DigiD',
            //'answer': 'Als je liever eerst even zonder DigiD wil kijken of je recht hebt op de individuele inkomstentoeslag hebben we ook een online check voor je gemaakt.'
            'interactiveAnswer': ['Als je liever eerst even zonder DigiD wil kijken of je recht hebt op de individuele inkomstentoeslag hebben we ook een ', { url: 'https://utrecht.nl/', text: 'online' }, ' check voor je gemaakt.']
        },
    ],
    'wizard': [
        {
            'question': 'Welke gegevens worden opgehaald en waarom?',
            'answer': formattedAnswer1
        }
    ],
}

export const explanationText: AccordionText = {
    'app-explanation': [
        {
            'question': 'Stap 1: Inloggen',
            'answer': 'Om de check of aanvraag voor extra geld te doen moet u inloggen met uw DigiD op MijnOverheid en bij de Belastingdienst. We krijgen de informatie dan automatisch. U hoeft ons zelf geen informatie meer te geven.Ook hoeft u geen formulieren naar ons op te sturen. '
        },
        {
            'question': 'Stap 2: Gegevens checken',
            'answer': `In deze stap bekijkt u of uw gegevens goed zijn.\nZijn uw gegevens goed? dan kunt u verder.\nZijn uw gegevens niet goed? neem contact op met uw gemeente.`
        }, {
            'question': 'Stap 3: Aanvraag versturen',
            'answer': 'In deze stap verstuurt u de aanvraag. De gemeente bekijkt of alles klopt. Klopt uw aanvraag? U ontvangt geld op uw rekening. Klopt uw aanvraag niet? Dan neemt de gemeente contact met u op.'
        }],
    'iit-explanation': [
        {
            'question': 'Wat is het?',
            'answer': `Hebt u drie jaar achter elkaar een laag inkomen en bent u jonger dan de AOW-leeftijd (66 jaar?) Misschien komt u dan in aanmerking voor de individuele inkomenstoeslag.\n
Dit is een geldbedrag dat u 1 keer in de 12 maanden kunt krijgen.\n
U betaalt geen belasting over deze individuele inkomenstoeslag en hoeft deze ook nooit meer terug te betalen.\n
Het wordt betaald door de gemeente.`
        },
        {
            'question': 'Hoeveel geld krijg ik?',
            'answer': `Het bedrag ligt tussen €52 en €849.\n
De hoogte is afhankelijk van de hoogte van uw inkomen en of een partner en kinderen heeft.\n
Deze app berekent het bedrag voor u.`
        }, {
            'question': 'Wanneer kan ik het aanvragen?',
            'answer': `U staat ingeschreven in Utrecht.\n
U leeft 3 jaar of langer van een laag inkomen.\n
Uw bezit weinig of geen vermogen.\n
U bent geen student en was ook geen student tijdens de 3 jaar dat u een laag inkomen had.\n
U bent 21 jaar of ouder, maar jonger dan de pensioengerechtigde leeftijd (AOW-leeftijd). Hebt u AOW, of heeft uw partner AOW? Dan krijgt u geen individuele inkomenstoeslag.\n
U kunt het maximaal 1 keer per 12 maanden aanvragen. Als u vorig jaar deze toeslag hebt gekregen ontvangt u een brief wanneer u het weer kunt aanvragen.`
        }]
}

