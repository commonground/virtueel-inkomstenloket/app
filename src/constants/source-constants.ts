type SourceTextMapValues = { title: string, info: string, intro: string, text: string }
export type SourceLogoKeys = 'moh-mb' | 'mbd-mb' | 'mpo-mb' | 'uwv-mb' | 'mts-mb' | 'duo-mb';

export const SOURCE_LOGO_MAP: Record<SourceLogoKeys, any> = {
    'moh-mb': require('../../assets/images/MOH-icon.png'),
    'mbd-mb': require('../../assets/images/MBD-icon.png'),
    'uwv-mb': require('../../assets/images/UWV-icon.png'),
    'mpo-mb': require('../../assets/images/MPO-icon.png'),
    'mts-mb': require('../../assets/images/MBD-icon.png'),
    'duo-mb': require('../../assets/images/MBD-icon.png'),
}

export const SOURCE_TEXT_MAP: Record<string, SourceTextMapValues> = {
    'moh-mb': {
        title: 'MijnOverheid',
        info: 'Identiteit en woning',
        intro: 'Log in bij de MijnOverheid om gegevens op te halen over uw identiteit en woonsituatie.',
        text: 'Gegevens over uw identiteit, burgerlijke staat en woonsituatie worden opgehaald bij MijnOverheid. '
    },
    'mbd-mb': {
        title: 'Belastingdienst',
        info: 'Laatste belastingaangifte',
        intro: 'Log in bij de Belastingdienst om gegevens op te halen over uw inkomsten en vermogen.',
        text: 'Gegevens over uw inkomsten en vermogen worden opgehaald bij de Belastingdienst.'
    },
    'uwv-mb': {
        title: 'UWV',
        info: 'Werk en inkomen',
        intro: 'Log in bij het UWV om gegevens op te halen over uw werk en inkomen.',
        text: 'Gegevens over uw werk en inkomen worden opgehaald bij het UWV.'
    },
    'mpo-mb': {
        title: 'Mijn Pensioenoverzicht',
        info: 'Pensioengegevens',
        intro: 'Log in bij Mijn Pensioenoverzicht om gegevens op te halen over uw pensioenen.',
        text: 'Gegevens over uw pensioen worden opgehaald bij Mijn Pensioenoverzicht.'
    },
    'mts-mb': {
        title: 'MijnToeslagen',
        info: "Toeslagen",
        intro: "Log in bij MijnToeslagen om gegevens op te halen over uw toeslagen.",
        text: "Gegevens over uw toeslagen worden opgehaald bij MijnToeslagen."
    },
    'duo-mb': {
        title: 'DUO',
        info: "Diploma's en studieschulden",
        intro: "Log in bij DUO om gegevens op te halen over uw diploma's en studieschulden.",
        text: "Gegevens over uw diploma's en studieschulden worden opgehaald bij DUO."
    }
}

export const SOURCES_LENGTH = Object.keys(SOURCE_TEXT_MAP).length;
