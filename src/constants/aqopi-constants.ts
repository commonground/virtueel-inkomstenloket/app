export enum AuthState {
    INITIAL = 'INITIAL',
    NOT_AUTHENTICATED = 'NOT_AUTHENTICATED',
    AUTHENTICATING = 'AUTHENTICATING',
    AUTHENTICATED = 'AUTHENTICATED'
}

const ConfigIds = [
    {
        id: 'moh-mb',
        data: [
            'persoonsgegevens',
            'familiegegevens',
            'nationaliteitgegevens',
            'id-gegevens',
            'diplomagegevens',
            'voertuiggegevens',
            'inkomensgegevens',
            // 'woz-gegevens',
            // 'kadastergegevens'
        ]
    },
    {
        id: 'mbd-mb',
        data: [
            'via',
            'inkomensverklaring'
        ]
    },
    {
        id: 'uwv-mb',
        data: ['verzekeringsbericht']
    },
    {
        id: 'mpo-mb',
        data: [
            'pensioen-xml',
            'pensioen-pdf'
        ]
    },
    {
        id: 'duo-mb',
        data: ['studieschuld']
    },
    {
        id: 'mts-mb',
        data: ['huishouden']
    }
]

const configurationEndpointStub = 'IWIZE-STUB';
const configurationEndpoint = 'IWIZE-PRODUCTION';

const parserEndpoint = 'https://parser-acct.aqopi.com/fin-advies-aqopi-parser.html';

const dataServiceEndpoint = 'https://data-services-v2.iwize.nl/api';

const consumerHostname = 'Merius';


export {
    configurationEndpoint,
    configurationEndpointStub,
    ConfigIds,
    consumerHostname,
    parserEndpoint,
    dataServiceEndpoint
};

