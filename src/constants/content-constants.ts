export enum ContentTypes {
    title = 'title',
    blueText = 'blueText',
    text = 'text',
    listItem = 'listItem',
    button = 'button',
    touchableListItem = 'touchableListItem'
}

type TextContent = {
    title: string;
    img?: any;
    step?: string;
    content: {
        type: ContentTypes;
        text?: string;
        smallText?: string;
        interactiveText?: (string | { url: string, text: string })[];
        // toScreen?: number;
        toUrl?: string,
        img?: any
    }[]
}

type FooterText = { title: string | null, text: string, url: string };

export const emailTitle: string = 'Vul uw e-mailadres in, dan ontvangt u een kopie van de aanvraag'

export const footerText: FooterText[] = [
    {
        title: 'Bel  ',
        text: '14 030',
        url: 'https://www.utrecht.nl/contact/verkort-telefoonnummer-gemeente'
    },
    {
        title: 'E-mail  ',
        text: 'geldhulp@utrecht.nl',
        url: 'https://www.utrecht.nl/contact/verkort-telefoonnummer-gemeente'
    },
    {
        title: null,
        text: 'Hoe werkt 14 030?',
        url: 'https://www.utrecht.nl/contact/verkort-telefoonnummer-gemeente'
    }
];

export const introContent: TextContent = {
    title: 'Krijg je extra geld van de gemeente?',
    img: require('../../assets/images/intro-img.png'),
    content: [
        {
            type: ContentTypes.text,
            text: 'Heeft u drie jaar lang of langer een laag inkomen? U kunt misschien extra geld van de gemeente krijgen (de Individuele Inkomenstoeslag).'
        },
        {
            type: ContentTypes.button,
            text: 'Start de uitleg'
        },
        {
            type: ContentTypes.blueText,
            text: 'Wilt u meteen aan de slag? Start dan hier de check.',
        }

    ]
};

export const explanationContent: TextContent = {
    title: 'Hoe werkt dit',
    content: [
        {
            type: ContentTypes.button,
            text: 'Start de check'
        }
    ]
};

export const applicationModeContent: TextContent = {
    title: 'Hoe wilt u de aanvraag doen?',
    content: [
        {
            type: ContentTypes.blueText,
            text: 'Ik doe de check alleen',
            img: require('../../assets/images/mode-solo.png'),
        },
        {
            type: ContentTypes.blueText,
            text: 'Ik doe de check samen met een hulpverlener',
            smallText: '(buurtteam, Gemeente Utrecht of VWN)',
            img: require('../../assets/images/mode-help.png'),
        }
    ]
};

export const loginContent: TextContent = {
    title: 'Inloggen',
    step: 'Stap 1 van 3',
    content: [
        {
            type: ContentTypes.text,
            text: 'Om te kijken of je extra geld van de gemeente kan krijgen hebben we bepaalde gegevens van je nodig. Als je bent ingelogd met DigiD kunnen we deze gegevens automatisch ophalen.'
        },
        {
            type: ContentTypes.title,
            text: 'Wat heb ik nodig?'
        },
        {
            type: ContentTypes.listItem,
            text: 'De DigiD inloggegevens'
        },
        {
            type: ContentTypes.listItem,
            text: 'Een eigen account bij MijnOverheid.nl'
        },
        {
            type: ContentTypes.title,
            text: 'DigiD of MijnOverheid.nl account aanvragen'
        },
        {
            type: ContentTypes.touchableListItem,
            text: 'Vraag uw DigiD aan via DigiD.nl',
            toUrl: 'https://www.digid.nl/digid-aanvragen-activeren'
        },
        {
            type: ContentTypes.touchableListItem,
            text: 'Vraag account aan via MijnOverheid.nl',
            toUrl: 'https://mijn.overheid.nl/'
        },
        {
            type: ContentTypes.button,
            text: 'Volgende'
        }
    ]
};

export const loginPartnerContent: TextContent = {
    title: 'Inloggen',
    step: 'Stap 1 van 3',
    content: [
        {
            type: ContentTypes.text,
            text: 'Uw gegevens hebben we. Nu hebben we de gegevens van uw partner nodig om te kijken of u extra geld krijgt.'
        },
        {
            type: ContentTypes.title,
            text: 'Wat heeft uw partner nodig?'
        },
        {
            type: ContentTypes.listItem,
            text: 'De DigiD inloggegevens'
        },
        {
            type: ContentTypes.listItem,
            text: 'Een eigen account bij MijnOverheid.nl'
        },
        {
            type: ContentTypes.title,
            text: 'DigiD of MijnOverheid.nl account aanvragen'
        },
        {
            type: ContentTypes.touchableListItem,
            text: 'Vraag uw DigiD aan via DigiD.nl',
            toUrl: 'https://www.digid.nl/digid-aanvragen-activeren'
        },
        {
            type: ContentTypes.touchableListItem,
            text: 'Vraag account aan via MijnOverheid.nl',
            toUrl: 'https://mijn.overheid.nl/'
        },
        {
            type: ContentTypes.button,
            text: 'Volgende'
        }
    ]
};

export const retrieveDataContent: Partial<TextContent> = {
    step: 'Stap 1 van 3 - Inloggen',
    content: [
        {
            type: ContentTypes.text,
            text: 'Het is gelukt om uw gegevens op te halen, deze kunt u in de volgende stap nakijken.'
        },
        {
            type: ContentTypes.button,
            text: 'Uw gegevens nakijken'
        }
    ]
};

export const startCheckContent: TextContent = {
    title: 'Hoe wilt u de aanvraag doen?',
    step: 'Stap 2 van 3 - Gegevens nakijken',
    content: [
        {
            type: ContentTypes.text,
            text: 'Bekijk of uw gegevens kloppen.'
        },
        {
            type: ContentTypes.text,
            text: 'Kloppen deze? Bekijk de uitslag, mogelijk krijgt u extra geld!'
        },
        {
            type: ContentTypes.text,
            text: 'Kloppen ze niet? Neem dan contact op met de gemeente.'
        },
        {
            type: ContentTypes.button,
            text: 'Volgende'
        }
    ]
};

export const verifyDataContent: TextContent = {
    title: 'Klopt uw informatie?',
    step: 'Stap 2 van 3 - Gegevens nakijken',
    content: [
        {
            type: ContentTypes.button,
            text: 'Alles klopt'
        },
        {
            type: ContentTypes.button,
            text: 'Er klopt iets niet'
        },

    ]
};

export const wrongDataContent: TextContent = {
    title: 'Er klopt iets niet',
    step: 'Stap 2 van 3',
    content: [
        {
            type: ContentTypes.text,
            interactiveText: ['Zijn de getoonde gegevens niet correct? Neem contact op met de gemeente via ', { url: 'https://www.utrecht.nl/contact/', text: 'e-mail' }, ', of bel ons op 14 030. U kunt de app sluiten.']
            //text: 'Zijn de getoonde gegevens niet correct? Neem contact op met de gemeente via e-mail, of bel ons op 14 030. U kunt de app sluiten.'
        },
        {
            type: ContentTypes.button,
            text: 'App sluiten'
        },

    ]
};

export const handToPartnerContent: TextContent = {
    title: 'Gegevens ontvangen',
    step: 'Stap 2 van 3 - Gegevens nakijken',
    content: [
        {
            type: ContentTypes.listItem,
            //text: 'Volgens de opgehaalde  gegevens heeft u recht op de Individuele Inkomenstoeslag (IIT).',
            text: 'U bent er bijna!'
        },
        {
            type: ContentTypes.listItem,
            text: 'We hebben uw gegevens ontvangen.'
        },
        {
            type: ContentTypes.listItem,
            text: 'Geef de telefoon aan uw partner zodat deze ook gegevens kan ophalen en nakijken.'
        },
        {
            type: ContentTypes.button,
            text: 'Volgende'
        },

    ]
};

export const handBackContent: TextContent = {
    title: 'Uitslag van de check',
    step: 'Stap 2 van 3 - Uitslag van de check',
    content: [
        {
            type: ContentTypes.listItem,
            text: 'U bent er bijna!',
        },
        {
            type: ContentTypes.listItem,
            text: 'We hebben uw gegevens ontvangen.'
        },
        {
            type: ContentTypes.listItem,
            text: 'Geef de telefoon terug aan uw partner zodat deze de uitslag van de check kan zijn.'
        },
        {
            type: ContentTypes.button,
            text: 'Resultaten bekijken'
        }
    ]
};

export const startApplicationHeader: string = 'Hieronder staat de samenvatting van uw aanvraag';

export const startApplicationContent: TextContent = {
    title: 'Aanvraag',
    step: 'Stap 3 van 3 - Uitslag van de check',
    content: [
        {
            type: ContentTypes.text,
            text: 'Volgens de gegevens hebben jullie recht op de Individuele Inkomenstoeslag (IIT).'
        },
        {
            type: ContentTypes.text,
            text: 'Een huishouden kan 1 keer per jaar een aanvraag voor deze toeslag doen.'
        },
        {
            type: ContentTypes.text,
            text: 'De gemeente checkt nog een keer of alles klopt zodat u extra geld kunt ontvangen op uw rekening'
        },
        {
            type: ContentTypes.text,
            text: 'Als alles klopt betaalt de gemeente de toeslag.'
        },
        {
            type: ContentTypes.button,
            text: 'Verstuur de aanvraag'
        },

    ]
};
export const startApplicationContent_NegativeResult: TextContent = {
    title: 'Aanvraag',
    step: 'Stap 3 van 3 - Uitslag van de check',
    content: [
        {
            type: ContentTypes.text,
            text: 'Volgens de gegevens hebben jullie geen recht op de Individuele Inkomenstoeslag (IIT).'
        },
        {
            type: ContentTypes.text,
            text: 'Een huishouden kan 1 keer per jaar een aanvraag voor deze toeslag doen.'
        },
        {
            type: ContentTypes.text,
            text: 'U kunt nu de app afsluiten'
        },
        {
            type: ContentTypes.button,
            text: 'App sluiten'
        },

    ]
};

export const applicationSentHeader: string = 'Binnen twee weken hoort u of u het bedrag ontvangt.';

export const applicationSentContent: TextContent = {
    title: 'We gaan het voor u regelen',
    content: [
        {
            type: ContentTypes.text,
            text: 'U kunt nu de app afsluiten'
        },
        {
            type: ContentTypes.button,
            text: 'App sluiten'
        },

    ]
};