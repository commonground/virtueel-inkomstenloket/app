import React, { useState } from 'react';
import { StyleSheet, View } from 'react-native';

// React navigation
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';

// Expo imports
import * as Font from 'expo-font';
import AppLoading from 'expo-app-loading';

// Import context
import { AppStateProvider } from './src/contexts/AppState';

// Import screens
import { IntroScreen } from './src/screens/intro-screen';
import { OverviewScreen } from './src/screens/data/overview-screen';
import { QueryScreen } from './src/screens/data/query-screen';
import { LoginScreen } from './src/screens/login-screen';
import { ApplicationModeScreen } from './src/screens/application-mode-screen';
import { StartCheckScreen } from './src/screens/check/start-check-sceen'; // Not in v0.3
import { VerifyDataScreen } from './src/screens/check/verify-data-screen';
import { WrongDataScreen } from './src/screens/check/wrong-data-screen';
import { CheckResultScreen } from './src/screens/check/check-result-screen';
import { StartApplicationScreen } from './src/screens/check/start-application-screen';
import { ApplicationSentScreen } from './src/screens/check/application-sent-screen';
import { ExplanationScreen } from './src/screens/explanation-screen';

// Create stack navigator
const Stack = createStackNavigator();


export default function App() {
  const [isLoadingComplete, setIsLoadingComplete] = useState(false);

  const loadResourcesAsync = async () => {
    return Font.loadAsync({
      'lucida-grande': require('./assets/fonts/LucidaGrande.ttf'),
      'lucida-grande-bold': require('./assets/fonts/LucidaGrandeBold.ttf'),
      'arial': require('./assets/fonts/ArialCE.ttf'),

    });
  };

  const handleLoadingError = (error: any) => {
    console.log('loading error', error);
  };

  const handleFinishLoading = () => {
    setIsLoadingComplete(true);
  };

  if (!isLoadingComplete) {
    return (
      <AppLoading
        startAsync={loadResourcesAsync}
        onError={handleLoadingError}
        onFinish={handleFinishLoading}
      />
    );
  } else {
    return (
      <AppStateProvider>
        <View style={styles.container}>
          <NavigationContainer>
            <Stack.Navigator screenOptions={{ headerShown: false, animationEnabled: false }} initialRouteName="intro">
              <Stack.Screen name="intro" component={IntroScreen} />
              <Stack.Screen name="app-explanation" component={ExplanationScreen} />
              <Stack.Screen name="iit-explanation" component={ExplanationScreen} />
              <Stack.Screen name="application-mode" component={ApplicationModeScreen} />
              <Stack.Screen name="login" component={LoginScreen} />
              <Stack.Screen name="overview" component={OverviewScreen} />
              <Stack.Screen name="query" component={QueryScreen} />
              {/* <Stack.Screen name="startCheck" component={StartCheckScreen} />  */}
              <Stack.Screen name="verifyData" component={VerifyDataScreen} />
              <Stack.Screen name="checkResult" component={CheckResultScreen} />
              <Stack.Screen name="wrongData" component={WrongDataScreen} />
              <Stack.Screen name="startApplication" component={StartApplicationScreen} />
              <Stack.Screen name="applicationSent" component={ApplicationSentScreen} />
            </Stack.Navigator>
          </NavigationContainer>
        </View>
      </AppStateProvider>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    marginTop: 40
  },
});
